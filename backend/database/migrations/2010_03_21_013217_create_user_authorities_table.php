<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_authorities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('authority_id')->unsigned();

            $table->bigInteger('user_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_authorities', function (Blueprint $table) {
            $table->dropForeign(['authority_id']);
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('user_authorities');
    }
}
