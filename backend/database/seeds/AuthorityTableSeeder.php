<?php

use App\Models\User\Authority;
use Illuminate\Database\Seeder;

class AuthorityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Authority();
        $role_admin->name = 'admin';
        $role_admin->save();

        $role_basic_user = new Authority();
        $role_basic_user->name = 'basic_user';
        $role_basic_user->save();
    }
}
