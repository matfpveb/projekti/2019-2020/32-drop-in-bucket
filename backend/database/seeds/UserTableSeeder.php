<?php

use App\Models\User\Authority;
use App\Models\User\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_authority = Authority::where('name', 'admin')->first();

        $admin = new User();
        $admin->email = 'admin@DropInBucket.com';
        $admin->password = Hash::make('123456');
        $admin->save();
        $admin->authorities()->attach($admin_authority );
    }
}
