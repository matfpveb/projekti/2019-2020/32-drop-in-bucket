## Podizanje u lokalu

### Podesavanje lokalnog okruzenja sa Vagrant-om i Homestead-om

Ovaj repozitorujom koristi Laravel Framework.

Ukoliko ste na Windows-u ili Mac-u mozete odmah preskociti uputstvo ne mogu vam pomoci..

- Instalirajte najnovije verzije Virtual Box-a i Vagrant-a (predpostavicu da imate  instaliran curl)

- Virtual Box: 
    ```
    sudo apt install virtualbox
    ```

- Ukoliko komanda ```vboxmanage --version``` vraca verziju manju od ```6.0``` skinite deb paket za Oracle Virtual Box verzije ```6.0```
i navise.
- Vagrant: 

    ```
    sudo apt update
    ```
    
    ```
    curl -O https://releases.hashicorp.com/vagrant/2.2.6/vagrant_2.2.6_x86_64.deb
    ```
    
    ```
    sudo apt install ./vagrant_2.2.6_x86_64.deb
    ```
    
- Nakon uspesne instalacije komanda ```vagrant --version``` bi trebalo da vraca ```Vagrant 2.2.6```

- Dodajte laravel Homestead box u Vagrant 
    ```
    vagrant box add laravel/homestead --box-version 9.3.0
    ```
- Skinite i inicializujte Homestead
    ```
    Skinuti sa linka https://github.com/laravel/homestead/releases/tag/v10.6.0 source
    Otpakovati i staviti sadrzaj u folder na nivou ~/ pod imenom Homestead
    cd ~/Homestead
    bash init.sh
    ```
- Mapirajte folder od projekta na vagrant okruzenje u Homestead.yaml
    ```
    folders:
       - map: /path/to/your/cloned/bucket_backend
         to: /home/vagrant/bucket_backend
    ```
- Mapirajte ime domena na vagrant-ov bucket_backend u Homestead.yaml 
    ```
    sites:
       - map: bucket-backend.test
         to: /home/vagrant/bucket_backend/public
    ```
- U vasem `/etc/hosts` mapirajte vase ime domena na vagrant ip adresu
    ```
    192.168.10.10  bucket-backend.test
    ```
- Napravite lokalni .env file za razvoj `cp .env.example .env`
- Modifikujte lokalni .env fajl:
    ```
    DB_HOST=192.168.10.10
    DB_PORT=3306
    DB_DATABASE=homestead
    DB_USERNAME=homestead
    DB_PASSWORD=secret
    ```
- Pozicionirajte se u vas Homestead folder
- Pokrenite vagrant sa `vagrant up`
- SSH u vagrant sa `vagrant ssh`
- Pozicionirajte se u vas bucket_backend folder unutar vagranta `cd /home/vagrant/bucket_backend`
- Instalirajte zavisnosti `composer install`
- Generisite app key: `php artisan key:generate`
- Generisite jwt key: `php artisan jwt:secret`
- Migrirajte bazu: `php artisan migrate`
- Seed-ujte bazu: `php artisan db:seed`
- Back ce vam biti dostupan na `http://bucket-backend.test`
- Ukoliko zelite da ocistite bazu kompletno: `php artisan migrate:fresh` zatim `php artisan db:seed`
