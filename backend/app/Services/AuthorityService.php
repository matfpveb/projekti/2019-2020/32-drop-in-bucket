<?php


namespace App\Services;

use App\Models\User\Authority;


class AuthorityService
{
    public function findByAuthorityName($name)
    {
        $authority = Authority::where('name', $name)->first();
        return $authority;
    }
}
