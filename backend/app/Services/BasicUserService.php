<?php


namespace App\Services;

use App\Models\User\BasicUser;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BasicUserService {

    private $authorityService;
    private $userService;

    public function __construct(AuthorityService $authorityService,
                                UserService $userService)
    {
        $this->authorityService = $authorityService;
        $this->userService = $userService;
    }

    public function create(Request $request)
    {
        $user_attributes = $request->only(['email', 'password']);

        $user = $this->userService->findByEmail(strtolower($user_attributes['email']));
        if ($user != null) {
            throw new BadRequestHttpException("User with given email already exists. ");
        }

        $user_attributes['password'] = Hash::make($user_attributes['password']);
        $user = User::create($user_attributes);
        $user->authorities()->attach($this->authorityService->findByAuthorityName("basic_user"));

        $basic_user_attributes = $request->only([
            'first_name',
            'last_name',
        ]);


        $basic_user_attributes['user_id'] = $user->id;
        $basic_user_attributes['storage_used'] = 0;

        BasicUser::create($basic_user_attributes);

        return $user;
    }

    public function findById(int $id)
    {
        return BasicUser::find($id);
    }


    public function update(array $attributes, $basic_user_id)
    {
        $basic_user = $this->findById($basic_user_id);

        if ($basic_user == null) {
            throw new BadRequestHttpException("Not found");
        }

        $basic_user->first_name = $attributes['first_name'];
        $basic_user->last_name = $attributes['last_name'];

        $basic_user->save();

        return $basic_user->user;
    }

    public function updateStorage($key, $size, $basic_user_id)
    {
        $basic_user = $this->findById($basic_user_id);

        if ($basic_user == null) {
            throw new BadRequestHttpException("Not found");
        }
        if($key == 'sub') {
            $basic_user->storage_used = $basic_user->storage_used - $size;
        }
        else if($key == 'add') {
            $basic_user->storage_used = $basic_user->storage_used + $size;
        }

        $basic_user->save();

        return $basic_user->user;
    }

}

