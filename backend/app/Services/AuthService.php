<?php


namespace App\Services;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Carbon\Carbon;

class AuthService {

    private $jwtAuth;

    public function __construct(JWTAuth $jwtAuth)
    {
        $this->jwtAuth = $jwtAuth;
    }

    public function login($credentials)
    {
        $token = auth()->attempt($credentials, ['exp' => Carbon::now()->addDays(30)->timestamp]);

        if (!$token) {
            throw new AccessDeniedHttpException('Login information incorrect.');
        }

        $user = $this->getLoggedInUser();

        $data = array(
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60,
            'user'         => $user,
        );

        return $data;
    }

    public function logout()
    {
        auth()->logout();
    }

    public function getLoggedInUser()
    {
        $user = auth()->user();

        if (!$user) {
            return null;
        }

        return $user;
    }


    public function authenticateUser(int $claimed_user_id)
    {
        $user = $this->getLoggedInUser();

        if ($user == null) {
            throw new AccessDeniedHttpException("User not logged in.");
        }

        if ($user->hasAuthority('admin'))
        {
            return $user;
        }

        if ($user->id !== $claimed_user_id) {
            throw new AccessDeniedHttpException("Unauthorized action.");
        }

        return $user;
    }

    public function authenticateBasicUser(int $claimed_basic_user_id)
    {
        $user = $this->getLoggedInUser();

        if ($user == null) {
            throw new AccessDeniedHttpException("User not logged in.");
        }

        if ($user->hasAuthority('admin'))
        {
            return $user;
        }

        if ($user->basic_user == null)
        {
            throw new BadRequestHttpException("Basic user not found.");
        }

        if ($user->basic_user->id !== $claimed_basic_user_id) {
            throw new AccessDeniedHttpException("Unauthorized action.");
        }

        return $user;
    }

    public function authorizeUser($authorities)
    {
        if ( $this->getLoggedInUser() == null)
        {
            throw new AccessDeniedHttpException("Unauthorized action. User not logged in.");
        }

        if (is_array($authorities)) {
            return $this->getLoggedInUser()->hasAnyAuthority($authorities) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->getLoggedInUser()->hasAuthority($authorities) ||
            abort(401, 'This action is unauthorized.');
    }
}
