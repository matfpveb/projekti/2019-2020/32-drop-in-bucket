<?php


namespace App\Services;
use App\Models\File\File;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\File as Facades_File;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Webpatser\Uuid\Uuid;

class FileService
{

    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function create($data, $file, $user_id)
    {
        // Check if user is claimed user of file
        if ($user_id != $data->owner_id)
        {
            throw new AccessDeniedHttpException("Action not authorized.");
        }

        // Upload file
        $original_file_name = $file->getClientOriginalName();
        $original_file_size = $file->getSize();
        preg_match('/\.([a-zA-Z0-9]+)$/', strtolower($original_file_name), $matches);
        $fileNameNoExtension = preg_replace("/\.[^.]+$/", "", $original_file_name);

        $file_type_ext = $matches[1];

        $filename = Uuid::generate()->string;
        $order_directory = 'users'.'/'.strval($user_id).'/files';
        $path = $order_directory.'/'.$filename.'.'.$file_type_ext;
        Storage::disk('local')->put($path, Facades_File::get($file));

        // Build creation object/attributes
        $attributes = (array) $data;
        $attributes['owner_id'] = $data->owner_id;
        $attributes['file_name'] = $fileNameNoExtension;
        $attributes['file_extension'] = $file_type_ext;
        $attributes['file_size'] = $original_file_size;
        $attributes['creation_date'] = Carbon::now();
        $attributes['file_path'] = $path;
        $attributes['state'] = 'basic';

        return File::create($attributes);
    }

    public function update(array $attributes, $file_id)
    {
        $file = $this->findById($file_id);

        if ($file == null) {
            throw new BadRequestHttpException("Not found");
        }

        $file->file_name = $attributes['name'];
        $file->state = $attributes['state'];

        $file->save();

        return $file;
    }

    public function duplicate($user_id, $file_id) {
        $file_data = $this->findById($file_id);

        $file = $file_data->file_path;

        $file_type_ext = $file_data->file_extension;

        $filename = Uuid::generate()->string;
        $order_directory = 'users'.'/'.strval($user_id).'/files';
        $path = $order_directory.'/'.$filename.'.'.$file_type_ext;
        Storage::disk('local')->copy($file, $path);

        $attributes['owner_id'] = $file_data->owner_id;
        $attributes['file_name'] = $file_data->file_name.' Copy';
        $attributes['file_extension'] = $file_type_ext;
        $attributes['file_size'] = $file_data->file_size;
        $attributes['creation_date'] = Carbon::now();
        $attributes['file_path'] = $path;
        $attributes['state'] = 'basic';

        return File::create($attributes);
    }

    public function findById($file_id)
    {
        $file = File::find($file_id);

        if ($file == null)
        {
            throw new BadRequestHttpException('File with given id not found.');
        }

        return $file;
    }

    public $userRoles = [
        'basic_user' => 'files.owner_id',
    ];

    public function findAllByUserId($user_id, $userRole)
    {
        if (!array_key_exists($userRole, $this->userRoles)) {
            throw new BadRequestHttpException("Bad user role requested");
        }


        $query = File::select('files.*')
            ->join('basic_user', 'basic_user.user_id', '=', 'files.owner_id')
            ->where($this->userRoles[$userRole], '=', $user_id)
            ->where('files.state', '!=', 'trashed');

        $content = $query->get();

        return $content;
    }

    public function findAllStarredByUserId($user_id, $userRole)
    {
        if (!array_key_exists($userRole, $this->userRoles)) {
            throw new BadRequestHttpException("Bad user role requested");
        }


        $query = File::select('files.*')
            ->join('basic_user', 'basic_user.user_id', '=', 'files.owner_id')
            ->where($this->userRoles[$userRole], '=', $user_id)
            ->where('files.state', '=', 'starred');

        $content = $query->get();

        return $content;
    }

    public function findAllTrashedByUserId($user_id, $userRole)
    {
        if (!array_key_exists($userRole, $this->userRoles)) {
            throw new BadRequestHttpException("Bad user role requested");
        }


        $query = File::select('files.*')
            ->join('basic_user', 'basic_user.user_id', '=', 'files.owner_id')
            ->where($this->userRoles[$userRole], '=', $user_id)
            ->where('files.state', '=', 'trashed');

        $content = $query->get();

        return $content;
    }



    public function findByUserIdAndId($user_id, $file_id)
    {
        $file = $this->findById($file_id);

        if ($file->owner_id != $user_id)
        {
            throw new BadRequestHttpException('Action not authorized.');
        }

        return $file;
    }

    public function getFile($file_path)
    {
        $file = null;
        try {
            $file = Storage::disk('local')->get($file_path);
        } catch (FileNotFoundException $e) {
            throw new BadRequestHttpException("Could not find requested file.");
        }

        return $file;
    }

    public function delete($file_id)
    {
        $file = $this->findById($file_id);
        $size = $file->file_size;
        if ($file->state != 'trashed')
        {
            throw new BadRequestHttpException('Action not authorized.');
        }
        Storage::disk('local')->delete($file->file_path);
        $file->delete();

        return $size;
    }
}
