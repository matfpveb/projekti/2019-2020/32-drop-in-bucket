<?php


namespace App\Models\User;
use Illuminate\Database\Eloquent\Model;

class BasicUser extends Model
{

    protected $table = 'basic_user';

    protected $fillable = [
        'user_id',

        'first_name',
        'last_name',
        'storage_used'
    ];

    protected $casts = [
        'user_id'           => 'integer',

        'first_name'        => 'string',
        'last_name'         => 'string',
        'storage_used'      => 'numeric'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User\User');
    }
}
