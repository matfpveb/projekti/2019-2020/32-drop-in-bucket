<?php


namespace App\Models\User;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{

    protected $table = 'users';

    protected $fillable = [
        'email',
        'password',
    ];

    protected $casts = [
        'email'        => 'string',
        'password'     => 'string',
    ];

    protected $hidden = [
        'password',
    ];


    // Deals with authentication

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }


    // Deals with user types
    public function basic_user()
    {
        return $this->hasOne('App\Models\User\BasicUser');
    }


    // Deals with users authority
    public function authorities()
    {
        return $this->belongsToMany('App\Models\User\Authority', 'user_authorities');
    }

    public function hasAnyAuthority($authorities)
    {
        return null !== $this->authorities()->whereIn('name', $authorities)->first();
    }

    public function hasAuthority($authority)
    {
        return null !== $this->authorities()->where('name', $authority)->first();
    }

    public function files()
    {
        return $this->hasMany('App\Models\File\File', 'owner_id');
    }
}
