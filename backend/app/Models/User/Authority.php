<?php


namespace App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Authority extends Model
{
    protected $table = 'authorities';

    protected $fillable = [
        'name',
    ];

    protected $casts = [
        'name' => 'string',
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User\User', 'user_authorities');
    }
}

