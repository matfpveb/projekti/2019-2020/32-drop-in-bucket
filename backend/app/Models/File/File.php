<?php


namespace App\Models\File;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'files';

    protected $fillable = [
        'owner_id',
        'file_name',
        'file_extension',
        'file_size',
        'creation_date',
        'file_path',
        'state'
    ];

    protected $casts = [
        'owner_id'                    => 'integer',
        'file_name'                   => 'string',
        'file_extension'              => 'string',
        'file_size'                   => 'numeric',
        'creation_date'               => 'date:d-m-Y',
        'file_path'                   => 'string',
        'state'                       => 'string',
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User\User', 'id', 'owner_id');
    }
}
