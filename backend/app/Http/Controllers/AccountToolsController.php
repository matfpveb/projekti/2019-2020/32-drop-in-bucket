<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;


class AccountToolsController extends Controller {

    use Helpers;

    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;

    }

    public function logout(Request $request, $user_id)
    {
        $this->authService->authorizeUser(['basic_user', 'admin']);
        $user = $this->authService->authenticateUser($user_id);

        $this->authService->logout();

        return response("Logged out.", 200);
    }



}
