<?php


namespace App\Http\Controllers;

use App\Services\AuthService;
use App\Services\BasicUserService;
use App\Services\FileService;
use App\Services\UserService;
use App\Transformers\FileTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FileController
{
    use Helpers;

    private $fileService;
    private $authService;
    private $userService;
    private $basicUserService;

    public function __construct(FileService $fileService,
                                AuthService $authService,
                                UserService $userService,
                                BasicUserService $basicUserService)
    {
        $this->fileService = $fileService;
        $this->authService = $authService;
        $this->userService = $userService;
        $this->basicUserService = $basicUserService;
    }

    public function create(Request $request, int $user_id)
    {
        $this->authService->authorizeUser(['basic_user']);
        $user = $this->authService->authenticateUser($user_id);

        $data_json = $request->input('data');
        $file = $request->file('file');

        $data = json_decode($data_json);

        Validator::make(
            (array) $data,
            [
                'owner_id'   => 'required|integer|gte:0',
            ],
            [
                'required'  => ':attribute is not provided.',
                'integer'   => ':attribute must be an integer.',
                'gte'       => ':attribute must be greater or equal to 0.',
            ],
            [
                'owner_id'   => 'Owner id',
            ]
        )->validate();

        Validator::make(
            $request->all(),
            [
                'file'   => 'required|file|max:102400',
            ],
            [
                'required'  => ':attribute is not provided.',
                'file'      => ':attribute must be a file format.',
                'max'       => 'Maximum size of :attribute is 100 Mb',
            ],
            [
                'file'   => 'File',
            ]
        )->validate();

        $file = $this->fileService->create($data, $file, $user_id);

        $this->basicUserService->updateStorage('add', $file->file_size, $user->basic_user->id);

        return $this->response->item($file, new FileTransformer());
    }

    public function update(Request $request, $user_id, $file_id)
    {
        $user = $this->authService->authenticateUser($user_id);
        $this->authService->authorizeUser(['basic_user']);
        $this->confirmFileOwnershipAndGet($user_id, $file_id);

        Validator::make(
            $request->all(),
            [
                'name' => 'required|string|min:1|max:200',

                'state' => 'required|string',
            ],
            [
                'required' => ':attribute field is not provided.',
                'string' => ':attribute field must be a string.',
            ],
            [
                'name' => 'File name',

                'state' => 'File state',
            ]
        )->validate();

        $file = $this->fileService->update($request->all(), $file_id);

        return $this->response->item($file, new FileTransformer());
    }

    public function confirmFileOwnershipAndGet($user_id, $file_id)
    {
        $user = $this->userService->findById($user_id);
        $file = $this->fileService->findById($file_id);

        if ($user == null || $file == null)
        {
            throw new BadRequestHttpException('Not found');
        }

        if ($user->id !== $file->owner_id)
        {
            throw new AccessDeniedHttpException('Unauthorized action.');
        }

        return $file;
    }

    public function findAll(int $user_id)
    {
        $this->authService->authorizeUser(['basic_user']);
        $user = $this->authService->authenticateUser($user_id);
        $data = $this->fileService->findAllByUserId($user_id, 'basic_user');

        return $this->response->item($data, new FileTransformer());
    }

    public function findAllStarred(int $user_id)
    {
        $this->authService->authorizeUser(['basic_user']);
        $user = $this->authService->authenticateUser($user_id);
        $data = $this->fileService->findAllStarredByUserId($user_id, 'basic_user');

        return $this->response->item($data, new FileTransformer());
    }

    public function findAllTrashed(int $user_id)
    {
        $this->authService->authorizeUser(['basic_user']);
        $user = $this->authService->authenticateUser($user_id);
        $data = $this->fileService->findAllTrashedByUserId($user_id, 'basic_user');

        return $this->response->item($data, new FileTransformer());
    }


    public function getFile(int $user_id, int $file_id)
    {
        $this->authService->authorizeUser(['basic_user']);
        $user = $this->authService->authenticateUser($user_id);

        $file = $this->fileService->findByUserIdAndId($user_id, $file_id);
        $rez_file = $this->fileService->getFile($file->file_path);

        return response()->streamDownload(function () use ($rez_file) {
            echo $rez_file;
        }, $file->file_name.'.'.$file->file_extension);
    }

    public function duplicate(int $user_id, int $file_id)
    {
        $this->authService->authorizeUser(['basic_user']);
        $user = $this->authService->authenticateUser($user_id);

        $file = $this->fileService->duplicate($user_id, $file_id);

        $this->basicUserService->updateStorage('add', $file->file_size, $user->basic_user->id);

        return $this->response->item($file, new FileTransformer());
    }

    public function delete(int $user_id, int $file_id)
    {
        $user = $this->authService->authenticateUser($user_id);
        $this->authService->authorizeUser(['basic_user']);
        $this->confirmFileOwnershipAndGet($user_id, $file_id);

        $size = $this->fileService->delete($file_id);


        $this->basicUserService->updateStorage('sub', $size, $user->basic_user->id);

        return response("", 200);
    }
}
