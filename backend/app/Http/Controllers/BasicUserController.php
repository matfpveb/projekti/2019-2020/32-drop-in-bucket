<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use App\Services\BasicUserService;

use App\Services\UserService;
use App\Transformers\BasicUserTransformer;
use Dingo\Api\Routing\Helpers;

use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BasicUserController extends Controller
{
    use Helpers;

    private $basicUserService;
    private $authService;
    private $logEntryService;
    private $userService;

    public function __construct(BasicUserService $basicUserService,
                                AuthService $authService,
                                UserService $userService)
    {
        $this->basicUserService = $basicUserService;
        $this->authService = $authService;
        $this->userService = $userService;
    }

    public function create(Request $request)
    {
        $this->validate(
            $request,
            [
                'email'         => 'required|email|min:6|max:100',
                'password'      => 'required|string|min:6|max:100',

                'first_name'    => 'required|string|min:2|max:100',
                'last_name'     => 'required|string|min:2|max:100',

            ]
        );

        $user = $this->basicUserService->create($request);
        if ($user) {
            return $this->response->item($user, new UserTransformer);
        }
        throw new BadRequestHttpException("Cannot create user.");
    }

    public function update(Request $request, $user_id, $basic_user_id)
    {
        $this->authService->authenticateBasicUser($basic_user_id);
        $this->validate(
            $request,
            [
                'first_name'    => 'required|string|min:2|max:100',
                'last_name'     => 'required|string|min:2|max:100',
            ]
        );

        $user = $this->basicUserService->update($request->all(), $basic_user_id);
        if ($user) {
            return $this->response->item($user, new UserTransformer);
        }
        throw new BadRequestHttpException("Cannot update user.");
    }

    public function getBasicUser($user_id, $basic_user_id) {
        $this->authService->authenticateBasicUser($basic_user_id);

        $basic_user = $this->basicUserService->findById($basic_user_id);
        if ($basic_user == null) {
            throw new BadRequestHttpException("Not found");
        }
        return $this->response->item($basic_user->user, new UserTransformer());
    }

}
