<?php


namespace App\Transformers;

use App\Models\User\BasicUser;
use League\Fractal\TransformerAbstract;

class BasicUserTransformer extends TransformerAbstract
{
    public function transform(BasicUser $basic_user)
    {
        if (empty($basic_user)) {
            return null;
        }
        return [
            'id'                => $basic_user->id,

            'first_name'        => $basic_user->first_name,
            'last_name'         => $basic_user->last_name,

            'email'             => $basic_user->user->email,
            'storage_used'     => $basic_user->storage_used,
        ];
    }
}

