<?php


namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use Exception;

class ExceptionTransformer extends TransformerAbstract
{
    public function transform(Exception $exception)
    {
        if (empty($exception)) {
            return null;
        }
        return [
            'error' => $exception->getMessage(),
            'code'  => $exception->getMessage(),
            'trace' => $exception->getTrace(),
        ];
    }
}
