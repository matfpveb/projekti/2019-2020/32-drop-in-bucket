<?php


namespace App\Transformers;
use App\Models\File\File;
use League\Fractal\TransformerAbstract;

class FileTransformer extends TransformerAbstract
{

    public function __construct()
    {
    }

    public function transform(File $file)
    {
        if (empty($file)){
            return null;
        }

        return [
            'id'                          => $file->id,
            'file_name'                   => $file->file_name,
            'file_extension'              => $file->file_extension,
            'file_size'                   => $file->file_size,
            'creation_date'               => $file->creation_date->format('d.m.Y.'),
            'state'                       => $file->state,
            'updated_at'                  => $file->updated_at,
        ];
    }
}
