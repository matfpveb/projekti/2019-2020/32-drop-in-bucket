<?php


namespace App\Transformers;

use App\Models\User\Authority;
use League\Fractal\TransformerAbstract;
class AuthorityTransformer extends TransformerAbstract
{
    public function transform(Authority $authority)
    {
        if (empty($authority)) {
            return null;
        }
        return [
            'id' => $authority->id,

            'name' => $authority->name,
        ];
    }
}
