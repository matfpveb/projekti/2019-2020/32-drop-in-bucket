<?php


namespace App\Transformers;

use App\Models\User\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'authorities',
        'basic_user',
    ];

    public function transform(User $user)
    {
        if (empty($user)){
            return null;
        }
        return [
            'id'            => $user->id,
            'email'         => $user->email,
        ];
    }

    public function includeAuthorities(User $user)
    {
        if (empty($user)){
            return null;
        }

        $authorities = $user->authorities()->get();

        return $this->collection($authorities, new AuthorityTransformer());
    }


    public function includeBasicUser(User $user)
    {
        if (empty($user)){
            return null;
        }

        $basic_user = $user->basic_user;

        if (empty($basic_user)){
            return null;
        }

        return $this->item($basic_user, new BasicUserTransformer());
    }
}
