<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Collection;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Collection::macro('toUsers', function () {
            return $this->map(function ($object) {
                return $object->user;
            });
        });

        Collection::macro('toSelf', function () {
            return $this->map(function ($object) {
                return $object;
            });
        });
    }
}
