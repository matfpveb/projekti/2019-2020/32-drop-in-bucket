<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app(Router::class);

$root_group = function (Router $api) {
    $api->group(['namespace' => 'App\Http\Controllers'], function (Router $api) {

        /* Public auth operations */
        $api->post('auth/login', 'AuthController@login');


        /* Public basic_user operations */
        $api->post('basicUser', 'BasicUserController@create');





        // Routes that require authentication
        $api->group(['middleware' => 'api'], function (Router $api) {

            /* Auth operations*/
            $api->put('users/{user_id}/basic_users/{basic_user_id}', 'BasicUserController@update')->where(['user_id' => '[0-9]+', 'basic_user_id' => '[0-9]+']);
            $api->get('users/{user_id}/basic_users/{basic_user_id}', 'BasicUserController@getBasicUser')->where(['user_id' => '[0-9]+', 'basic_user_id' => '[0-9]+']);
            $api->post('users/{user_id}/logout',            'AccountToolsController@logout')->where(['user_id' => '[0-9]+']);

            $api->post('users/{user_id}/files',             'FileController@create')->where(['user_id' => '[0-9]+']);
            $api->get('users/{user_id}/files/basic',        'FileController@findAll')->where(['user_id' => '[0-9]+']);
            $api->get('users/{user_id}/files/starred',      'FileController@findAllStarred')->where(['user_id' => '[0-9]+']);
            $api->get('users/{user_id}/files/trashed',      'FileController@findAllTrashed')->where(['user_id' => '[0-9]+']);
            $api->post('users/{user_id}/files/{file_id}/duplicate',    'FileController@duplicate')->where(['user_id' => '[0-9]+', 'file_id' => '[0-9]+']);
            $api->get('users/{user_id}/files/{file_id}',    'FileController@getFile')->where(['user_id' => '[0-9]+', 'file_id' => '[0-9]+']);
            $api->delete('users/{user_id}/files/{file_id}', 'FileController@delete')->where(['user_id' => '[0-9]+', 'file_id' => '[0-9]+']);
            $api->put('users/{user_id}/files/{file_id}',    'FileController@update')->where(['user_id' => '[0-9]+', 'file_id' => '[0-9]+']);
        });
    });
};

$api->version('v1', $root_group);
