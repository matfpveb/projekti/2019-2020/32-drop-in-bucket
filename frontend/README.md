# DropInBucket Frontend

## Informacije o podesavanju:
#### Podesavanje Debian/Ubuntu okruzenja
- Za podesavaje Debian/Ubuntu okruzenja potrebno je instalirati Node.js, npm i opciono yarn. Mozete koristiti sledece komande:

```
sudo apt-get install curl
```

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
```

- Ukoliko zelite neku drugu verziju noda potrebno je samo promeniti ```setup_10.x``` u npr. ```setup_12.x```

```
sudo apt install nodejs
```
- Ukoliko su sve komande uspesno prosle rezultat poziva ```node --version``` ce izgledati slicno ovome:
```v10.16.3```
- Takodje rezultat ```npm --version``` ce izgledati slicno ovome: ```6.14.3```

- Opciono mozete intalirati po zelji yarn uz pomoc sledecih komandi:

```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
```
 
```
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
```

```
sudo apt update && sudo apt install yarn
```

- Ako je sve uspesno proslo imacete spremno okruzenje za pokretanje App-a

#### Podesavanje za druga Linux okruzenja, Mac i Windows
- You're on your own

## Pokretanje projekta

- Posto ste klonirali repozitorijum pozicionirajte se u njega.

- Izgradite komandom: 
```
npm install
```

- Pokrenite komandom: 
```
npm run start
```
ili 
```
yarn start
```
- Dinner is served at: http://localhost:3000/

- (Moze biti i na drugoj adresi ukoliko je port 3000 zauzet, u tom slucaju ce vas upozoriti npm i recice vam na kojoj adresi mozete pritupiti)