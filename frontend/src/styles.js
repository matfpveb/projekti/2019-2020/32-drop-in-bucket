
import styled from 'styled-components'

const AlignRight = styled.div`
  text-align: right;
`;

export { AlignRight }
