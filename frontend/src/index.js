import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'

import './index.css'
import 'antd/dist/antd.css'

import { auth } from './utils'
import { routes } from './routes'
import { App } from './containers'
import { configureStore, history } from './store/configureStore'

const store = configureStore();
auth.setStore(store);
routes.setStore(store);

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App routes={routes.all} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
