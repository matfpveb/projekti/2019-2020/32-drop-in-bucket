import {
    AUTH_SET_USER,
    AUTH_UPDATE_USER,
    AUTH_UPDATING_USER,
    AUTH_SET_LOGGING_IN,
    AUTH_SET_REGISTERING,
    AUTH_SET_STORAGE
} from '../actions/actionTypes'

import initialState from './initialState'

const authReducer = (state = initialState.auth, action) => {
    switch (action.type) {
        case AUTH_SET_USER:
            return {
                ...state,
                user: action.payload,
            };

        case AUTH_UPDATE_USER:
            return {
                ...state,
                user: action.payload,
            };

        case AUTH_UPDATING_USER:
            return {
                ...state,
                isUpdatingUser: action.payload,
            }

        case AUTH_SET_LOGGING_IN:
            return {
                ...state,
                loggingIn: action.payload,
            };

        case AUTH_SET_REGISTERING:
            return {
                ...state,
                registering: action.payload,
            };

        case AUTH_SET_STORAGE:
            return {
                ...state,
                storage: action.payload,
            };

        default:
            return state
    }
};

export default authReducer
