import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import authReducer from './authReducer'
import fileReducer from "./fileReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  router: routerReducer,
  files: fileReducer
})

export default rootReducer
