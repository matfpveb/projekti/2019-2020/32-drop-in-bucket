import { storage } from '../utils'

const authData = storage.retrieveAuthData();

const initialState = {
  auth: {
    user: authData ? authData.user.data : null,
    isUpdatingUser: false,
    loggingIn: false,
    registering: false,
    redirect: null,
    storage: authData ? (authData.user.data.basic_user ? authData.user.data.basic_user.data.storage_used : null) : null,
  },
  files: {
    isFetchingAllFiles: false,
    allFiles: [],
    fetchedAllFiles: false,
    isFetchingStarredFiles: false,
    starredFiles: [],
    fetchedStarredFiles: false,
    isFetchingTrashedFiles: false,
    trashedFiles: [],
    fetchedTrashedFiles: false,
    currentUploadingFileName: '',
    isFileUploading: false,
    fileUploadProgress: 0,
    hadUploadError: false,
    currentDownloadingFileName: '',
    isFileDownloading: false,
    fileDownloadProgress: 0,
    fileDataBlob: null,
    fetchingFileData: false
  }
};

export default initialState
