import {
    FILES_SET_ALL_FILES, FILES_SET_FETCHING_ALL_FILES,
    FILES_SET_STARRED_FILES, FILES_SET_FETCHING_STARRED_FILES,
    FILES_SET_TRASHED_FILES, FILES_SET_FETCHING_TRASHED_FILES,
    FILES_SET_FETCHED_ALL_FILES, FILES_SET_FETCHED_STARRED_FILES,
    FILES_SET_FETCHED_TRASHED_FILES, FILES_SET_FILE_UPLOADING,
    FILES_SET_FILE_UPLOADING_ERROR, FILES_SET_FILE_UPLOAD_PROGRESS,
    FILES_TRASH_FILE, FILES_STARRED_FILE,
    FILES_BASIC_FROM_STARRED_FILE, FILES_BASIC_FROM_TRASHED_FILE,
    FILES_DELETE_FILE, FILES_SET_FILE_DOWNLOADING, FILES_SET_FILE_DOWNLOAD_PROGRESS,
    FILES_SET_GETTING_FILE_DATA, FILES_FILE_DATA,
    FILES_SET_NEW_UPLOAD_FILE_NAME, FILES_SET_NEW_DOWNLOAD_FILE_NAME, FILES_RENAME_BASIC_FILE,
    FILES_RENAME_STARRED_FILE, FILES_UPDATE_MODIFICATION_TIME_TRASHED, FILES_UPDATE_MODIFICATION_TIME_BASIC,
    FILES_UPDATE_MODIFICATION_TIME_STARRED
} from '../actions/actionTypes'

import initialState from './initialState'

const fileReducer = (state = initialState.files, action) => {
    switch (action.type) {
        case FILES_SET_FETCHING_ALL_FILES:
            return {
                ...state,
                isFetchingAllFiles: action.payload,
            };

        case FILES_SET_ALL_FILES:
            return {
                ...state,
                allFiles: action.payload,
            };

        case FILES_SET_FETCHED_ALL_FILES:
            return {
                ...state,
                fetchedAllFiles: action.payload,
            };

        case FILES_SET_FETCHING_STARRED_FILES:
            return {
                ...state,
                isFetchingStarredFiles: action.payload,
            };

        case FILES_SET_STARRED_FILES:
            return {
                ...state,
                starredFiles: action.payload,
            };

        case FILES_SET_FETCHED_STARRED_FILES:
            return {
                ...state,
                fetchedStarredFiles: action.payload,
            };

        case FILES_SET_FETCHING_TRASHED_FILES:
            return {
                ...state,
                isFetchingTrashedFiles: action.payload,
            };

        case FILES_SET_TRASHED_FILES:
            return {
                ...state,
                trashedFiles: action.payload,
            };

        case FILES_SET_FETCHED_TRASHED_FILES:
            return {
                ...state,
                fetchedTrashedFiles: action.payload,
            };

        case FILES_SET_FILE_UPLOADING:
            return {
                ...state,
                isFileUploading: action.payload,
            };
        case FILES_SET_FILE_UPLOAD_PROGRESS:
            return {
                ...state,
                fileUploadProgress: action.payload,
            };

        case FILES_SET_FILE_UPLOADING_ERROR:
            return {
                ...state,
                hadUploadError: action.payload,
            };

        case FILES_TRASH_FILE:
            return {
                ...state,
                allFiles: state.allFiles.filter(
                    i => i.id !== action.payload.id
                ),
                starredFiles: state.starredFiles.filter(
                    i => i.id !== action.payload.id
                ),
                trashedFiles: [...state.trashedFiles, action.payload].sort(function(a, b){
                    if (a.id < b.id) {
                        return 1;
                    }
                    if (a.id > b.id) {
                        return -1;
                    }
                    return 0;
                })
            };

        case FILES_STARRED_FILE:
            return {
                ...state,
                allFiles: state.allFiles.map(
                    i => {
                        if(i.id === action.payload.id) {
                            let copy_item = {...i};
                            copy_item.state = 'starred';
                            return copy_item;
                        }
                        else
                            return i;
                    }
                ),
                starredFiles: [...state.starredFiles, action.payload].sort(function(a, b){
                    if (a.id < b.id) {
                        return 1;
                    }
                    if (a.id > b.id) {
                        return -1;
                    }
                    return 0;
                })
            };

        case FILES_BASIC_FROM_STARRED_FILE:
            return {
                ...state,
                allFiles: state.allFiles.map(
                    i => {
                        if(i.id === action.payload.id) {
                            let copy_item = {...i};
                            copy_item.state = 'basic';
                            return copy_item;
                        }
                        else
                            return i;
                    }
                ),
                starredFiles: state.starredFiles.filter(
                    i => i.id !== action.payload.id
                )
            };

        case FILES_BASIC_FROM_TRASHED_FILE:
            return {
                ...state,
                allFiles: [...state.allFiles, action.payload].sort(function(a, b){
                    if (a.id < b.id) {
                        return 1;
                    }
                    if (a.id > b.id) {
                        return -1;
                    }
                    return 0;
                }),
                trashedFiles: state.trashedFiles.filter(
                    i => i.id !== action.payload.id
                )
            };

        case FILES_DELETE_FILE:
            return {
                ...state,
                trashedFiles: state.trashedFiles.filter(
                    i => i.id !== action.payload
                )
            };

        case FILES_SET_FILE_DOWNLOADING:
            return {
                ...state,
                isFileDownloading: action.payload,
            };

        case FILES_SET_FILE_DOWNLOAD_PROGRESS:
            return {
                ...state,
                fileDownloadProgress: action.payload,
            };

        case FILES_SET_GETTING_FILE_DATA:
            return {
                ...state,
                fetchingFileData: action.payload,
            };

        case FILES_FILE_DATA:
            return {
                ...state,
                fileDataBlob: action.payload,
            }

        case FILES_SET_NEW_UPLOAD_FILE_NAME:
            return {
                ...state,
                currentUploadingFileName: action.payload,
            };

        case FILES_SET_NEW_DOWNLOAD_FILE_NAME:
            return {
                ...state,
                currentDownloadingFileName: action.payload,
            };

        case FILES_RENAME_BASIC_FILE:
            return {
                ...state,
                allFiles: state.allFiles.map(
                    i => {
                        if(i.id === action.payload.id) {
                            return action.payload;
                        }
                        else
                            return i;
                    }
                )
            };

        case FILES_RENAME_STARRED_FILE:
            return {
                ...state,
                allFiles: state.allFiles.map(
                    i => {
                        if(i.id === action.payload.id) {
                            return action.payload;
                        }
                        else
                            return i;
                    }
                ),
                starredFiles: state.starredFiles.map(
                    i => {
                        if(i.id === action.payload.id) {
                            return action.payload;
                        }
                        else
                            return i;
                    }
                ),
            };
        case FILES_UPDATE_MODIFICATION_TIME_TRASHED:
            return {
                ...state,
                trashedFiles: state.trashedFiles.map(
                    i => {
                        if(i.id === action.payload.id) {
                            let copy_item = {...i};
                            copy_item.updated_at = action.payload.updated_at;
                            return copy_item;
                        }
                        else
                            return i;
                    }
                ),
            };
        case FILES_UPDATE_MODIFICATION_TIME_BASIC:
            return {
                ...state,
                allFiles: state.allFiles.map(
                    i => {
                        if(i.id === action.payload.id) {
                            let copy_item = {...i};
                            copy_item.updated_at = action.payload.updated_at;
                            return copy_item;
                        }
                        else
                            return i;
                    }
                ),
            };
        case FILES_UPDATE_MODIFICATION_TIME_STARRED:
            return {
                ...state,
                allFiles: state.allFiles.map(
                    i => {
                        if(i.id === action.payload.id) {
                            let copy_item = {...i};
                            copy_item.updated_at = action.payload.updated_at;
                            return copy_item;
                        }
                        else
                            return i;
                    }
                ),
                starredFiles: state.starredFiles.map(
                    i => {
                        if(i.id === action.payload.id) {
                            let copy_item = {...i};
                            copy_item.updated_at = action.payload.updated_at;
                            return copy_item;
                        }
                        else
                            return i;
                    }
                ),
            };


        default:
            return state
    }
};

export default fileReducer
