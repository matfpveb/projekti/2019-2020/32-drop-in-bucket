import App from './App'
import LoginPage from './LoginPage'
import RegisterPage from './RegisterPage'

export {
  App,
  LoginPage,
  RegisterPage
}
