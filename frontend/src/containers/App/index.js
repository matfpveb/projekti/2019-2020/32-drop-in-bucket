import React from 'react'
import { Switch, Route } from 'react-router-dom'
import './style.css'
import { PrivateRoute } from '../../routes/helpers'

const App = ({ routes }) => (
  <Switch>
    {routes.map(
      (route, index) =>
        route.isPublic ? (
          <Route
            key={index}
            exact={route.exact}
            path={route.path}
            component={route.component}
          />
        ) : (
          <PrivateRoute
            key={index}
            exact={route.exact}
            path={route.path}
            component={route.component}
          />
        )
    )};
  </Switch>
);

export default App
