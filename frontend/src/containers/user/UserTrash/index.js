import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Layout, Card, Avatar, Row, Col, Menu, Dropdown, Button, Spin, notification, Progress } from "antd";
import { updateStorage, logoutUser } from '../../../actions/authActions'
import {
    fetchTrashedFiles, fetchStarredFiles, updateFile,
    deleteFile, fetchAllFiles, uploadNewFile
} from '../../../actions/fileActions'
import { Sidebar, UserHeader } from "../../../components";
import {
    DeleteOutlined, UndoOutlined,
    CaretDownFilled, FolderFilled, FileTextFilled, FilePdfFilled,
    FileWordFilled, FileExcelFilled, SoundFilled, VideoCameraFilled,
    FileZipFilled, FileImageFilled, FileUnknownFilled, FileTextTwoTone,
    FilePdfTwoTone, FileWordTwoTone, FileExcelTwoTone, SoundTwoTone,
    VideoCameraTwoTone, FileZipTwoTone, FileImageTwoTone, FileUnknownTwoTone,
    LoadingOutlined
} from "@ant-design/icons";

import './style.css'
const { Meta } = Card;

class UserTrash extends Component {
    state = {
        file: null,
        percentage: 0,
        option: 'normal',
        name: '',
        downloadPercentage: 0,
        downloadOption: 'normal',
        downloadName: '',
        filesForShow: []
    };

    triggerSidebarUpload = (file) => {
        const { user } = this.props
        this.setState({
            file: file,
            name: file.name,
        });

        const formData = new FormData()
        formData.append('file', file)
        const data = {
            owner_id: user.id,
        }
        formData.set('data', JSON.stringify(data))
        this.props.uploadNewFile({formData: formData, user_id: user.id, name: file.name}).then(()=>{
            if(!this.props.hadUploadError) {
                this.props.fetchAllFiles({user_id: this.props.user.id})
                this.props.updateStorage({user_id: user.id, basic_user_id: user.basic_user.data.id})
            }
        })
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
        if (this.props.hadUploadError === false && nextProps.hadUploadError === true) {
            this.setState({
                option: 'exception',
                percentage: 0,
                name: nextProps.currentUploadingFileName
            }, ()=> {
                this.openNotification()
            })
        }
        else if (nextProps.isFileUploading) {
            this.openNotification()
            if (nextProps.fileUploadProgress < 100) {
                this.setState({
                    option: 'normal',
                    percentage: nextProps.fileUploadProgress.toFixed(0),
                    name: nextProps.currentUploadingFileName
                }, ()=> {
                    this.openNotification()
                })
            }
            if (nextProps.fileUploadProgress === 100) {
                this.setState({
                    option: 'success',
                    percentage: 100,
                    name: nextProps.currentUploadingFileName
                }, ()=> {
                    this.openNotification()
                })
            }
        }

        if (nextProps.isFileDownloading) {
            this.openDownloadNotification()
            if (nextProps.fileDownloadProgress < 100) {
                this.setState({
                    downloadOption: 'normal',
                    downloadPercentage: nextProps.fileDownloadProgress.toFixed(0),
                    downloadName: nextProps.currentDownloadingFileName
                }, ()=> {
                    this.openDownloadNotification()
                })
            }
            if (nextProps.fileDownloadProgress === 100) {
                this.setState({
                    downloadOption: 'success',
                    downloadPercentage: 100,
                    downloadName: nextProps.currentDownloadingFileName
                }, ()=> {
                    this.openDownloadNotification()
                })
            }
        }
    }

    openNotification = () => {
        notification.open({
            key: 'uploadKey',
            message: 'Uploading',
            description:
                <React.Fragment>
                    <Progress
                        type={'circle'}
                        strokeColor={{
                            '0%': '#108ee9',
                            '100%': '#87d068',
                        }}
                        status={this.state.option}
                        percent={this.state.percentage}
                        width={50}
                        style={{marginRight: 10}}
                    />
                    {this.state.name}
                </React.Fragment>,
            placement: 'bottomRight',
            duration: 0,
        });
    };

    openDownloadNotification = () => {
        notification.open({
            key: 'downloadKey',
            message: 'Downloading',
            description:
                <React.Fragment>
                    <Progress
                        type={'circle'}
                        strokeColor={{
                            '0%': '#108ee9',
                            '100%': '#87d068',
                        }}
                        status={this.state.downloadOption}
                        percent={this.state.downloadPercentage}
                        width={50}
                        style={{marginRight: 10}}
                    />
                    {this.state.downloadName}
                </React.Fragment>,
            placement: 'bottomRight',
            duration: 0
        });
    };

    componentDidMount() {
        if(this.props.items.length === 0 && this.props.fetchedTrashedFiles === false) {
            this.initialize()
            this.initializeOther()
        } else {
            this.setState({
                filesForShow: this.props.items
            })
        }
    }

    initialize = () => {
        this.props.fetchTrashedFiles({user_id: this.props.user.id})
            .then(() => {
                this.setState({
                    filesForShow: this.props.items
                })
            })
    }

    initializeOther = () => {
        this.props.fetchStarredFiles({user_id: this.props.user.id})
        this.props.fetchAllFiles({user_id: this.props.user.id})
    }

    getAvatar = (extension) => {

        switch (extension) {
            case 'json':
            case 'txt':
            case 'csv':
            case 'text':
            case 'py':
            case 'c':
            case 'cpp':
                return <FileTextFilled/>
            case 'pdf':
                return <FilePdfFilled/>
            case 'docx':
            case 'doc':
                return <FileWordFilled/>
            case 'xls':
            case 'xlsx':
            case 'ods':
                return <FileExcelFilled/>
            case 'mp3':
            case 'ogg':
            case 'wav':
                return <SoundFilled/>
            case 'mp4':
            case 'mov':
            case 'mkv':
                return <VideoCameraFilled/>
            case 'zip':
            case '7z':
            case 'rar':
                return <FileZipFilled/>
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'svg':
                return <FileImageFilled/>
            default:
                return <FileUnknownFilled />

        }
    }

    getIcon = (extension) => {

        switch (extension) {
            case 'json':
            case 'txt':
            case 'csv':
            case 'text':
            case 'py':
            case 'c':
            case 'cpp':
                return <FileTextTwoTone/>
            case 'pdf':
                return <FilePdfTwoTone/>
            case 'docx':
            case 'doc':
                return <FileWordTwoTone/>
            case 'xls':
            case 'xlsx':
            case 'ods':
                return <FileExcelTwoTone/>
            case 'mp3':
            case 'ogg':
            case 'wav':
                return <SoundTwoTone/>
            case 'mp4':
            case 'mov':
            case 'mkv':
                return <VideoCameraTwoTone/>
            case 'zip':
            case '7z':
            case 'rar':
                return <FileZipTwoTone/>
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'svg':
                return <FileImageTwoTone/>
            default:
                return <FileUnknownTwoTone />
        }
    }

    handleFileSearch = (filteredItems) => {
        if (filteredItems.length === 0) {
            this.setState({
                filesForShow: this.props.items
            })
        } else {
            this.setState({
                filesForShow: filteredItems
            })
        }
    }

    handleRestore = (item) => {
        this.props.updateFile({user_id: this.props.user.id, name: item.file_name, file_id: item.id, item: item, state: 'basic'})
    }

    handleDelete = (item) => {
        this.props.deleteFile({user_id: this.props.user.id, file_id: item.id}).then(()=>{
            this.props.updateStorage({user_id: this.props.user.id, basic_user_id: this.props.user.basic_user.data.id})
        });
    }

    handleEmptyTrash = () => {
        let items = [...this.props.items];
        let len = items.length;
        let pom = 1;
        items.forEach(item => {
            if (pom === len) {
                this.props.deleteFile({user_id: this.props.user.id, file_id: item.id}).then(()=>{
                    this.props.updateStorage({user_id: this.props.user.id, basic_user_id: this.props.user.basic_user.data.id})
                })
            }
            else {
                this.props.deleteFile({user_id: this.props.user.id, file_id: item.id})
            }
            pom++;
        })

    }

    render() {
        return (
            <Layout>
                <UserHeader
                    history={this.props.history}
                    onLogout={this.props.logoutUser}
                    filesForSearch={this.props.items}
                    page={"trash"}
                    performSearch={this.handleFileSearch}
                />
                <Layout>
                    <Sidebar
                        history={this.props.history}
                        storage={this.props.storage}
                        triggerSidebarUpload={this.triggerSidebarUpload}
                    />
                    <Layout>
                        <Layout.Content>
                            <Card style={{height: 'calc(100vh - 65px)', overflow: 'auto'}} bodyStyle={{padding: "0"}}>
                                <div style={{marginTop: 10, paddingBottom: 10, borderBottom: '1px solid rgb(88, 88, 88)'}}>
                                <Dropdown
                                    overlay={
                                        <Menu style={{width:200}}>
                                            <Menu.Item key="1" style={{fontSize: 16}} onClick={()=>{this.handleEmptyTrash()}}>Empty trash</Menu.Item>
                                        </Menu>
                                    }
                                    trigger={['click']}
                                >
                                    <Button className={'trash-title'} type={'primary'} size={'large'} style={{fontSize: 18, paddingLeft: 8, paddingRight: 8, borderRadius: 10, marginLeft: 11, border: 0}}>
                                        Trash <CaretDownFilled style={{fontSize: 15}}/>
                                    </Button>
                                </Dropdown>
                                </div>
                                <div style={{marginTop: 10, marginLeft: 20, fontSize: 16, fontWeight: 'bold'}}>
                                    Folders
                                </div>
                                <Row gutter={20} style={{marginRight: 40, marginLeft: 10, marginTop: 15}}>
                                    {this.props.folders.map( item =>
                                        <Col xs={{span:24}} sm={{span:12}} md={{span:8}} lg={{span:6}} xl={{span:6}} xxl={{span:4}} style={{marginBottom: 20}} key={item.id}>
                                            <Dropdown
                                                overlay={
                                                    <Menu style={{width:200}}>
                                                        <Menu.Item  key="1"><UndoOutlined style={{fontSize: 16}}/>Restore</Menu.Item>
                                                        <Menu.Item  key="2"><DeleteOutlined  style={{fontSize: 16}}/>Delete Forever</Menu.Item>
                                                    </Menu>
                                                }
                                                trigger={['contextMenu']}>
                                                <Button
                                                    className={'folder-button'}
                                                    size={'large'}
                                                    onClick={()=>{
                                                        console.log('click')
                                                    }}
                                                    onDoubleClick={()=>{
                                                        console.log('double click')
                                                    }}
                                                >
                                                    <FolderFilled style={{fontSize: 20, marginTop: 2}}/>Folder {item.id}
                                                </Button>
                                            </Dropdown>
                                        </Col>
                                    )}
                                </Row>
                                <div style={{marginTop: 10, marginLeft: 20, fontSize: 16, fontWeight: 'bold'}}>
                                    Files
                                </div>
                                <Spin size={'large'} className={"spin"} indicator={<LoadingOutlined style={{ fontSize: 90 }} spin />} spinning={this.props.isFetchingTrashedFiles}>
                                <Row gutter={20} style={{marginRight: 40, marginLeft: 10, marginTop: 20, minHeight: '55vh'}}>
                                    {this.state.filesForShow.map(item =>
                                            <Col xs={{span: 24}} sm={{span: 12}} md={{span: 8}} lg={{span: 6}}
                                                 xl={{span: 6}} xxl={{span: 4}} style={{marginBottom: 20}} key={item.id}>
                                                <Dropdown
                                                    overlay={
                                                        <Menu style={{width:200}}>
                                                            <Menu.Item  key="1" onClick={()=>{this.handleRestore(item)}}><UndoOutlined style={{fontSize: 16}}/>Restore</Menu.Item>
                                                            <Menu.Item  key="2" onClick={()=>{this.handleDelete(item)}}><DeleteOutlined  style={{fontSize: 16}}/>Delete Forever</Menu.Item>
                                                        </Menu>
                                                    }
                                                    trigger={['contextMenu']}
                                                >
                                                    <Card
                                                        style={{ width: '100%'}}
                                                        bodyStyle={{ padding: 0 }}
                                                        cover={
                                                            <Avatar
                                                                icon={this.getAvatar(item.file_extension)}
                                                                style={{fontSize: 150, height: '120%', paddingTop: 10, paddingBottom: 10}}
                                                                shape={'square'}
                                                            />
                                                        }
                                                    >
                                                        <Meta
                                                            avatar={<Avatar
                                                                icon={this.getIcon(item.file_extension)}
                                                                shape={'square'}
                                                                size={'large'}
                                                            />}
                                                            style={{padding: 10}}
                                                            title={`${item.file_name}.${item.file_extension}`}
                                                            description={<div style={{marginTop: -10}}>Added {item.creation_date}</div>}
                                                        />
                                                    </Card>
                                                </Dropdown>
                                            </Col>
                                        )}
                                </Row>
                                </Spin>
                            </Card>
                        </Layout.Content>
                    </Layout>
                </Layout>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    items: state.files.trashedFiles,
    folders: [{id:1},{id:2}],
    currentUploadingFileName: state.files.currentUploadingFileName,
    isFileUploading: state.files.isFileUploading,
    fileUploadProgress: state.files.fileUploadProgress,
    hadUploadError: state.files.hadUploadError,
    currentDownloadingFileName: state.files.currentDownloadingFileName,
    isFileDownloading: state.files.isFileDownloading,
    fileDownloadProgress: state.files.fileDownloadProgress,
    isFetchingTrashedFiles: state.files.isFetchingTrashedFiles,
    fetchedTrashedFiles: state.files.fetchedTrashedFiles,
    user: state.auth.user,
    storage: state.auth.storage
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
      logoutUser, fetchTrashedFiles, updateFile,
      updateStorage, uploadNewFile, deleteFile,
      fetchAllFiles, fetchStarredFiles
  }, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UserTrash)
)
