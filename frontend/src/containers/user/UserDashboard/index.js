import React, {Component, Fragment} from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack'
import {
    Layout, Card, Dropdown, Button, Row, Col,
    Avatar, Menu, Empty, Modal, Upload, Progress,
    notification, Spin, Form, Input, Drawer
} from "antd";
import { updateStorage, logoutUser } from '../../../actions/authActions'
import {
    fetchAllFiles, fetchStarredFiles,
    fetchTrashedFiles, updateFile,
    uploadNewFile, downloadFile, getFileData,
    duplicateFile, renameFile } from '../../../actions/fileActions'
import {Sidebar, UserHeader} from "../../../components";
import './style.css'
import {
    CaretDownFilled, CopyOutlined, DeleteOutlined, DownloadOutlined,
    EditOutlined, EyeOutlined, InfoCircleOutlined,
    FileTextFilled, StarOutlined, FolderAddOutlined, FolderOpenOutlined,
    FileOutlined, FolderFilled, InboxOutlined, LoadingOutlined, FilePdfFilled,
    FileWordFilled, FileExcelFilled, SoundFilled, VideoCameraFilled,
    FileZipFilled, FileImageFilled, FileUnknownFilled, FileTextTwoTone,
    FilePdfTwoTone, FileWordTwoTone, FileExcelTwoTone, SoundTwoTone,
    VideoCameraTwoTone, FileZipTwoTone, FileImageTwoTone, FileUnknownTwoTone,
    StarFilled
} from "@ant-design/icons";

const { Meta } = Card;

const { Dragger } = Upload;

const folder_menu = (
    <Menu style={{width:200}}>
        <Menu.Item key="1"><StarOutlined style={{fontSize: 16}}/>Add to Starred</Menu.Item>
        <Menu.Item style={{borderBottom: '1px solid rgb(88, 88, 88)'}} key="2"><EditOutlined style={{fontSize: 16}}/>Rename</Menu.Item>
        <Menu.Item style={{marginTop: 5}} key="3"><InfoCircleOutlined style={{fontSize: 16}}/>View Details</Menu.Item>
        <Menu.Item style={{borderBottom: '1px solid rgb(88, 88, 88)'}} key="4"><DownloadOutlined style={{fontSize: 16}}/>Download</Menu.Item>
        <Menu.Item style={{marginTop: 5}} key="5"><DeleteOutlined style={{fontSize: 16}}/>Remove</Menu.Item>
    </Menu>
);

const options = {
    cMapUrl: 'cmaps/',
    cMapPacked: true,
};

let dragging = 0;

class UserDashboard extends Component {
    state = {
        file: null,
        isFileOver: false,
        percentage: 0,
        option: 'normal',
        name: '',
        downloadPercentage: 0,
        downloadOption: 'normal',
        downloadName: '',
        renameItem: null,
        isRenameModalVisible: false,
        previewItem: null,
        isPreviewModalVisible: false,
        isDetailsVisible: false,
        detailsItem: null,
        width: window.innerWidth * 0.9,
        height: window.innerHeight * 0.9,
        imageWidth: 0,
        imageHeight: 0,
        fileUrl: '',
        numPages: 1,
        pageNumber: 1,
        pageWidth: 0,
        pageHeight: 0,
        files: [],
        filesForShow: [],
    };

    handleDragEnter = e => {
        dragging++

        e.stopPropagation();
        e.preventDefault();

        let dt = e.dataTransfer;
        if (dt.types != null && ((dt.types.length && dt.types[0] === 'Files'))) {
            if(!this.state.isFileOver) {
                this.setState(state => {
                    return {
                        isFileOver: true,
                    };
                })
            }
        }
        return false
    };

    handleDragLeave = e => {

        dragging--;
        if (dragging === 0) {
            this.setState(state => {
                return {
                    isFileOver: false,
                };
            })
        }

        e.stopPropagation();
        e.preventDefault();
        return false
    };

    handleDrop = e => {
        e.preventDefault();
    };

    triggerUpload = () => {
        const { user } = this.props

        const formData = new FormData()
        formData.append('file', this.state.file)
        const data = {
            owner_id: user.id,
        }
        formData.set('data', JSON.stringify(data))
        this.props.uploadNewFile({formData: formData, user_id: user.id, name: this.state.name}).then(()=>{
            if(!this.props.hadUploadError) {
                this.initialize()
                this.props.updateStorage({user_id: user.id, basic_user_id: user.basic_user.data.id})
            }
        })
    }

    triggerSidebarUpload = (file) => {
        const { user } = this.props
        this.setState({
            file: file,
            name: file.name,
        });

        const formData = new FormData()
        formData.append('file', file)
        const data = {
            owner_id: user.id,
        }
        formData.set('data', JSON.stringify(data))
        this.props.uploadNewFile({formData: formData, user_id: user.id, name: file.name}).then(()=>{
            if(!this.props.hadUploadError) {
                this.initialize()
                this.props.updateStorage({user_id: user.id, basic_user_id: user.basic_user.data.id})
            }
        })
    }

    triggerFileDuplicate = (item) => {
        const { user } = this.props
        this.props.duplicateFile({user_id: user.id, file_id: item.id})
            .then(() => {
                this.initialize()
                this.props.updateStorage({user_id: user.id, basic_user_id: user.basic_user.data.id})
            })
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
        if (this.props.hadUploadError === false && nextProps.hadUploadError === true) {
            this.setState({
                option: 'exception',
                percentage: 0,
                name: nextProps.currentUploadingFileName
            }, ()=> {
                this.openNotification()
            })
        }
        else if (nextProps.isFileUploading) {
            this.openNotification(nextProps.currentUploadingFileName)
            if (nextProps.fileUploadProgress < 100) {
                this.setState({
                    option: 'normal',
                    percentage: nextProps.fileUploadProgress.toFixed(0),
                    name: nextProps.currentUploadingFileName
                }, ()=> {
                    this.openNotification()
                })
            }
            if (nextProps.fileUploadProgress === 100) {
                this.setState({
                    option: 'success',
                    percentage: 100,
                    name: nextProps.currentUploadingFileName
                }, ()=> {
                    this.openNotification()
                })
            }
        }

        if (nextProps.isFileDownloading) {
            this.openDownloadNotification()
            if (nextProps.fileDownloadProgress < 100) {
                this.setState({
                    downloadOption: 'normal',
                    downloadPercentage: nextProps.fileDownloadProgress.toFixed(0),
                    downloadName: nextProps.currentDownloadingFileName
                }, ()=> {
                    this.openDownloadNotification()
                })
            }
            if (nextProps.fileDownloadProgress === 100) {
                this.setState({
                    downloadOption: 'success',
                    downloadPercentage: 100,
                    downloadName: nextProps.currentDownloadingFileName
                }, ()=> {
                    this.openDownloadNotification()
                })
            }
        }

        if (this.props.fileDataBlob !== nextProps.fileDataBlob) {
            if (this.state.previewItem.file_extension === 'pdf') {
                this.setState({
                    fileUrl: URL.createObjectURL(nextProps.fileDataBlob)
                })
            } else {
                let image = document.getElementById('slika');
                let img = new Image();
                img.src = URL.createObjectURL(nextProps.fileDataBlob);
                image.src = img.src;
                img.onload = () => {
                    this.setState({
                        imageWidth: img.width,
                        imageHeight: img.height,
                    })
                    this.setImageSize(img.height, img.width)
                }
            }
        }
    }

    openNotification = () => {
        notification.open({
            key: 'uploadKey',
            message: 'Uploading',
            description:
                <React.Fragment>
                    <Progress
                        type={'circle'}
                        strokeColor={{
                            '0%': '#108ee9',
                            '100%': '#87d068',
                        }}
                        status={this.state.option}
                        percent={this.state.percentage}
                        width={50}
                        style={{marginRight: 10}}
                    />
                    {this.state.name}
                </React.Fragment>,
            placement: 'bottomRight',
            duration: 0,
        });
    };

    openDownloadNotification = () => {
        notification.open({
            key: 'downloadKey',
            message: 'Downloading',
            description:
                <React.Fragment>
                    <Progress
                        type={'circle'}
                        strokeColor={{
                            '0%': '#108ee9',
                            '100%': '#87d068',
                        }}
                        status={this.state.downloadOption}
                        percent={this.state.downloadPercentage}
                        width={50}
                        style={{marginRight: 10}}
                    />
                    {this.state.downloadName}
                </React.Fragment>,
            placement: 'bottomRight',
            duration: 0
        });
    };

    componentDidMount() {
        window.addEventListener('contextmenu', function(event){
            event.preventDefault();})
        document.addEventListener("dragenter", this.handleDragEnter);
        document.addEventListener("dragleave", this.handleDragLeave);
        window.addEventListener("drop", this.handleDrop, false);
        window.addEventListener("resize", this.handleResize);

        if(this.props.items.length === 0 && this.props.fetchedAllFiles === false) {
            this.initialize()
            this.initializeOther()
        } else {
            this.setState({
                filesForShow: this.props.items
            })
        }
    }

    initialize = () => {
        this.props.fetchAllFiles({user_id: this.props.user.id})
            .then(() => {
                this.setState({
                    filesForShow: this.props.items
                })
            })
    }

    initializeOther = () => {
        this.props.fetchStarredFiles({user_id: this.props.user.id})
        this.props.fetchTrashedFiles({user_id: this.props.user.id})
    }

    setImageSize = (height, width) => {
        let windowWidth = window.innerWidth * 0.85
        let windowHeight = window.innerHeight * 0.85
        if (windowHeight < height) {
            let diff = windowHeight / height
            height = diff * height
            width = diff * width
        }
        if (windowWidth < width) {
            let diff = windowWidth / width
            height *= diff
            width *= diff
        }
        this.setState({
            imageWidth: width,
            imageHeight: height
        })
    }

    handleResize = e => {
        if (this.state.isPreviewModalVisible) {
            this.setImageSize(this.state.imageHeight, this.state.imageWidth)
        }
        this.setState({
            width: window.innerWidth * 0.9,
            height: window.innerHeight * 0.9
        })
    }

    handleTrash = (item) => {
        this.props.updateFile({user_id: this.props.user.id, name: item.file_name, file_id: item.id, item: item, state: 'trashed'})
    }

    handleAddStarred = (item) => {
        this.props.updateFile({user_id: this.props.user.id, name: item.file_name, file_id: item.id, item: item, state: 'starred'})
    }

    handleRemoveStarred = (item) => {
        this.props.updateFile({user_id: this.props.user.id, name: item.file_name, file_id: item.id, item: item, state: 'basic'})
    }

    handleDownloadFile = (item) => {
        this.setState({
            downloadName: `${item.file_name}.${item.file_extension}`,
        });
        this.props.downloadFile({user_id: this.props.user.id, file_id: item.id, item: item, name: `${item.file_name}.${item.file_extension}`})
    }

    handleFileRename = (item) => {
        this.setState({
            renameItem: item,
            isRenameModalVisible: true
        })
    }

    handleRenameClose = () => {
        this.setState({
            isRenameModalVisible: false
        })
    }

    onRename = (values) => {
        this.setState({
            renameItem: {...this.state.renameItem, file_name: values.file_name},
            isRenameModalVisible: false,
        }, ()=>{
            this.props.renameFile({user_id: this.props.user.id, file_id: this.state.renameItem.id, item: this.state.renameItem})
        })
    }

    handleFileSearch = (filteredItems) => {
        if (filteredItems.length === 0) {
            this.setState({
                filesForShow: this.props.items
            })
        } else {
            this.setState({
                filesForShow: filteredItems
            })
        }
    }

    handleFilePreview = (item) => {
        this.setState({
            previewItem: item,
            isPreviewModalVisible: true
        });
        this.props.getFileData({user_id: this.props.user.id, file_id: item.id})
    }

    handlePreviewClose = () => {
        this.setState({
            isPreviewModalVisible: false,
            pageNumber: 1
        })
    }

    onDocumentLoadSuccess = ({numPages}) => {
        this.setState({
            numPages: numPages
        })
    }

    previousPage = () => {
        this.changePage(-1);
    }

    nextPage = () => {
        this.changePage(1);
    }

    changePage = offset => this.setState(prevState => ({
        pageNumber: (prevState.pageNumber || 1) + offset,
    }));

    setPageScale = () => {
        let scale = 1;
        if  (this.state.pageHeight > this.state.height) {
            scale = this.state.height / this.state.pageHeight
        }
        if (this.state.pageWidth * scale > this.state.width){
            scale *= this.state.width / (this.state.pageWidth * scale)
        }
        return scale
    }

    handleFileDetails = (item) => {
        this.setState({
            detailsItem: item,
            isDetailsVisible: true,
        })
    }

    onDetailsClose = () => {
        this.setState({
            isDetailsVisible: false,
        })
    }

    formatBytes = (a,b= 2) => {
        if(0===a)
            return"0 Bytes";
        const c = 0 > b ? 0:b, d = Math.floor(Math.log(a)/Math.log(1024));
        return parseFloat((a/Math.pow(1024,d)).toFixed(c))+" "+["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"][d]
    }

    formatBytesWithCommas = (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }


    componentWillUnmount() {
        window.removeEventListener('contextmenu', function(event){
            event.preventDefault();})
        document.removeEventListener("dragenter", this.handleDragEnter);
        document.removeEventListener("dragleave", this.handleDragLeave);
        window.removeEventListener("drop", this.handleDrop, false);
        window.removeEventListener("resize", this.handleResize);
    }

    getAvatar = (extension) => {

        switch (extension) {
            case 'json':
            case 'txt':
            case 'csv':
            case 'text':
            case 'py':
            case 'c':
            case 'cpp':
                return <FileTextFilled/>
            case 'pdf':
                return <FilePdfFilled/>
            case 'docx':
            case 'doc':
                return <FileWordFilled/>
            case 'xls':
            case 'xlsx':
            case 'ods':
                return <FileExcelFilled/>
            case 'mp3':
            case 'ogg':
            case 'wav':
                return <SoundFilled/>
            case 'mp4':
            case 'mov':
            case 'mkv':
                return <VideoCameraFilled/>
            case 'zip':
            case '7z':
            case 'rar':
                return <FileZipFilled/>
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'svg':
                return <FileImageFilled/>
            default:
                return <FileUnknownFilled />

        }
    }

    getIcon = (extension) => {

        switch (extension) {
            case 'json':
            case 'txt':
            case 'csv':
            case 'text':
            case 'py':
            case 'c':
            case 'cpp':
                return <FileTextTwoTone/>
            case 'pdf':
                return <FilePdfTwoTone/>
            case 'docx':
            case 'doc':
                return <FileWordTwoTone/>
            case 'xls':
            case 'xlsx':
            case 'ods':
                return <FileExcelTwoTone/>
            case 'mp3':
            case 'ogg':
            case 'wav':
                return <SoundTwoTone/>
            case 'mp4':
            case 'mov':
            case 'mkv':
                return <VideoCameraTwoTone/>
            case 'zip':
            case '7z':
            case 'rar':
                return <FileZipTwoTone/>
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'svg':
                return <FileImageTwoTone/>
            default:
                return <FileUnknownTwoTone />
        }
    }

    getType = (extension) => {

        switch (extension) {
            case 'json':
            case 'txt':
            case 'csv':
            case 'text':
            case 'py':
            case 'c':
            case 'cpp':
                return 'Text File'
            case 'pdf':
                return 'PDF'
            case 'docx':
            case 'doc':
                return 'Word Document'
            case 'xls':
            case 'xlsx':
            case 'ods':
                return 'Excel Document'
            case 'mp3':
            case 'ogg':
            case 'wav':
                return 'Audio'
            case 'mp4':
            case 'mov':
            case 'mkv':
                return 'Video'
            case 'zip':
            case '7z':
            case 'rar':
                return 'Compressed Archive'
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'svg':
                return 'Image'
            default:
                return 'Unknown'
        }
    }

    parseDate = (date) => {
        return new Date(Date.parse(date)).toLocaleString('en-GB')
    }




    render() {
        return (
            <Layout>
                <UserHeader
                    history={this.props.history}
                    onLogout={this.props.logoutUser}
                    filesForSearch={this.props.items}
                    page={"dashboard"}
                    performSearch={this.handleFileSearch}
                />
                <Layout>
                    <Sidebar
                        history={this.props.history}
                        storage={this.props.storage}
                        triggerSidebarUpload={this.triggerSidebarUpload}
                    />
                    <Layout>
                        <Layout.Content>
                            <Card style={{height: 'calc(100vh - 65px)', overflow: 'auto'}} bodyStyle={{padding: "0"}} >
                                <div style={{marginTop: 10, paddingBottom: 10, borderBottom: '1px solid rgb(88, 88, 88)'}}>
                                    <Dropdown
                                        overlay={
                                            <Menu style={{width:200}}>
                                                <Menu.Item key="1" style={{fontSize: 16, borderBottom: '1px solid rgb(88, 88, 88)'}}><FolderAddOutlined style={{fontSize: 16}} />New Folder</Menu.Item>

                                                <Menu.Item key="2" style={{padding: 0}}>
                                                    <Upload
                                                        name = {'file'}
                                                        beforeUpload={(file) => {
                                                            this.triggerSidebarUpload(file)
                                                            return false;
                                                        }}
                                                        multiple = {false}
                                                        showUploadList = {false}
                                                    >
                                                        <Button className={'menuUploadButton'} style={{width: 200, margin: 0, padding: 0, textAlign: 'left'}}><FileOutlined style={{fontSize: 16, marginLeft: 11}} /><p style={{fontSize: 16, marginLeft: 8, display: 'inline'}}>Upload Files</p></Button>
                                                    </Upload>
                                                </Menu.Item>

                                                <Menu.Item key="3" style={{fontSize: 16}}><FolderOpenOutlined style={{fontSize: 16}} />Upload Folder</Menu.Item>
                                            </Menu>
                                        }
                                        trigger={['click']}
                                    >
                                        <Button className={'dash-title'} type={'primary'} size={'large'} style={{fontSize: 18, paddingLeft: 8, paddingRight: 8, borderRadius: 10, marginLeft: 11, border: 0}}>
                                            My bucket <CaretDownFilled style={{fontSize: 15}}/>
                                        </Button>
                                    </Dropdown>
                                </div>
                                {this.props.folders.length === 0 && this.props.items.length === 0 ?
                                    (<React.Fragment>
                                        <Empty
                                            imageStyle={{
                                                transform: `scale(3.0)`
                                            }}
                                            style={{
                                                position: 'absolute',
                                                top: '50%',
                                                left: '50%',
                                                transform: 'translate(-50%,-50%)'
                                            }}
                                        />
                                    </React.Fragment>)
                                    :
                                    (<React.Fragment>
                                        {this.props.folders.length === 0 ? null :
                                            (<React.Fragment>
                                                <div style={{marginTop: 10, marginLeft: 20, fontSize: 16, fontWeight: 'bold'}}>
                                                    Folders
                                                </div>
                                                <Row gutter={20} style={{marginRight: 40, marginLeft: 10, marginTop: 15}}>
                                                    {this.props.folders.map( item =>
                                                        <Col xs={{span:24}} sm={{span:12}} md={{span:8}} lg={{span:6}} xl={{span:6}} xxl={{span:4}} style={{marginBottom: 20}} key={item.id}>
                                                            <Dropdown overlay={folder_menu} trigger={['contextMenu']}>
                                                                <Button
                                                                    className={'folder-button'}
                                                                    size={'large'}
                                                                    onClick={()=>{
                                                                        console.log('click')
                                                                    }}
                                                                    onDoubleClick={()=>{
                                                                        console.log('double click')
                                                                    }}
                                                                >
                                                                    <FolderFilled style={{fontSize: 20, marginTop: 2}}/>Folder {item.id}
                                                                </Button>
                                                            </Dropdown>
                                                        </Col>
                                                    )}
                                                </Row>
                                            </React.Fragment>)
                                        }
                                        <React.Fragment>
                                            <div style={{marginTop: 10, marginLeft: 20, fontSize: 16, fontWeight: 'bold'}}>
                                                Files
                                            </div>
                                            <Spin size={'large'} className={"spin"} indicator={<LoadingOutlined style={{ fontSize: 90 }} spin />} spinning={this.props.isFetchingAllFiles}>
                                            <Row gutter={20} style={{marginRight: 40, marginLeft: 10, marginTop: 20, minHeight: '55vh'}}>
                                                {this.state.filesForShow.map( item =>
                                                    <Col xs={{span:24}} sm={{span:12}} md={{span:8}} lg={{span:6}} xl={{span:6}} xxl={{span:4}} style={{marginBottom: 20}} key={item.id}>
                                                        <Dropdown
                                                            overlay={
                                                                <Menu style={{width:200}}>
                                                                    {(this.getType(item.file_extension)) === 'Image' || (this.getType(item.file_extension)) === 'PDF' ?
                                                                        <Menu.Item style={{borderBottom: '1px solid rgb(88, 88, 88)'}} key="1" onClick={() => {this.handleFilePreview(item)}}><EyeOutlined style={{fontSize: 16}}/>Preview</Menu.Item>
                                                                        : null
                                                                    }
                                                                    {item.state !== 'starred' ?
                                                                        <Menu.Item style={{marginTop: 5}} key="2" onClick={()=>{this.handleAddStarred(item)}}><StarOutlined style={{fontSize: 16}}/>Add to Starred</Menu.Item>
                                                                        :
                                                                        <Menu.Item style={{marginTop: 5}} key="2" onClick={()=>{this.handleRemoveStarred(item)}}><StarFilled style={{fontSize: 16}}/>Remove from Starred</Menu.Item>
                                                                    }
                                                                    <Menu.Item style={{borderBottom: '1px solid rgb(88, 88, 88)'}} key="3" onClick={()=>{this.handleFileRename(item)}}><EditOutlined style={{fontSize: 16}}/>Rename</Menu.Item>
                                                                    <Menu.Item style={{marginTop: 5}} key="4" onClick={()=>{this.handleFileDetails(item)}}><InfoCircleOutlined style={{fontSize: 16}}/>View Details</Menu.Item>
                                                                    <Menu.Item key="5" onClick={()=>{this.triggerFileDuplicate(item)}}><CopyOutlined style={{fontSize: 16}}/>Make Copy</Menu.Item>
                                                                    <Menu.Item style={{borderBottom: '1px solid rgb(88, 88, 88)'}} key="6" onClick={()=>{this.handleDownloadFile(item)}}><DownloadOutlined style={{fontSize: 16}}/>Download</Menu.Item>
                                                                    <Menu.Item style={{marginTop: 5}} key="7" onClick={()=>{this.handleTrash(item)}}><DeleteOutlined style={{fontSize: 16}}/>Remove</Menu.Item>
                                                                </Menu>
                                                            }
                                                            trigger={['contextMenu']}
                                                        >
                                                            <Card
                                                                style={{ width: '100%'}}
                                                                bodyStyle={{ padding: 0 }}
                                                                cover={
                                                                    <React.Fragment>
                                                                        {item.state === 'starred' ?
                                                                        <StarFilled style={{color: "#f9d71c", fontSize:35, paddingTop: 5, paddingLeft: 5, textAlign:'left'}}/>
                                                                        : null
                                                                        }
                                                                            <Avatar
                                                                                icon={this.getAvatar(item.file_extension)}
                                                                                style={{fontSize: 150, height: '120%',marginTop: item.state === 'starred' ? -40 : 0, paddingTop: 10, paddingBottom: 10}}
                                                                                shape={'square'}
                                                                            />
                                                                    </React.Fragment>
                                                                }
                                                            >
                                                                <Meta
                                                                    avatar={<Avatar
                                                                        icon={this.getIcon(item.file_extension)}
                                                                        shape={'square'}
                                                                        size={'large'}
                                                                    />}
                                                                    style={{padding: 10}}
                                                                    title={`${item.file_name}.${item.file_extension}`}
                                                                    description={<div style={{marginTop: -10}}>Added {item.creation_date}</div>}
                                                                />
                                                            </Card>
                                                        </Dropdown>
                                                    </Col>
                                                )}
                                            </Row>
                                        </Spin>
                                        </React.Fragment>
                                    </React.Fragment>)}
                            </Card>
                            <Modal
                                centered={true}
                                visible={this.state.isFileOver}
                                width={'99%'}
                                footer={null}
                                closable={false}
                                bodyStyle={{height: '97vh', padding: 3, backgroundColor: 'rgba(255,255,255,0.15)' }}
                                className={"uploadModal"}
                            >
                                <Dragger
                                    name = {'file'}
                                    beforeUpload={(file) => {
                                        this.setState({
                                            file: file,
                                            name: file.name,
                                            isFileOver: false,
                                        }, ()=>{this.triggerUpload()});
                                        return false;
                                    }}
                                    style={{minHeight: '95vh', backgroundColor: 'rgba(0,0,0,0.25)'}}
                                    multiple = {false}
                                    showUploadList = {false}
                                >
                                    <p className="ant-upload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                    <p className="ant-upload-hint">
                                        Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                                        band files
                                    </p>
                                </Dragger>

                            </Modal>

                            <Modal
                                centered={true}
                                visible={this.state.isRenameModalVisible}
                                onCancel={this.handleRenameClose}
                                footer={null}
                                title={'Rename'}
                            >
                                <div style={{fontSize: 14, marginTop: -5, marginBottom: 5, fontWeight: 'bold'}}>
                                    New file name:
                                </div>
                                <Form
                                    name={'renameForm'}
                                    onFinish={this.onRename}
                                    style={{marginBottom: -20}}
                                >
                                    <Form.Item
                                        name="file_name"
                                        rules={[{ required: true, message: 'Please enter the new file name!' },
                                            {type: 'string', min: 1, message: 'File name too short!'},
                                            {type: 'string', max: 200, message: 'File name too long!'}]}
                                        validateTrigger={'onSubmit'}
                                        initialValue={this.state.renameItem ? this.state.renameItem.file_name : ''}
                                    >
                                        <Input
                                            type={'string'}
                                            placeholder="File Name"
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <div align={'right'}>
                                            <Button type="primary" htmlType="submit">
                                                Rename
                                            </Button>
                                        </div>
                                    </Form.Item>
                                </Form>
                            </Modal>

                            <Modal
                                visible={this.state.isPreviewModalVisible}
                                onCancel={this.handlePreviewClose}
                                footer={null}
                                width={this.state.width}
                                style={{ top: 20 }}
                                bodyStyle={{height: this.state.height * 1.06}}

                            >
                                {this.props.fetchingFileData === true
                                    ? (<Spin size={'large'}
                                             className={"spin"}
                                             indicator={<LoadingOutlined style={{fontSize: 90}} spin/>}
                                             spinning={this.props.fetchingFileData}
                                             style={{height: this.state.height * 0.75, marginTop: this.state.height * 0.25, marginLeft: 'auto', marginRight: 'auto', display: 'block'}}/>)
                                    : (this.state.previewItem !== null && this.getType(this.state.previewItem.file_extension) === 'Image'
                                        ? (<img alt="Image" id="slika"
                                                style={{width: this.state.imageWidth,
                                                        height: this.state.imageHeight,
                                                        marginLeft: 'auto', marginRight: 'auto', display: 'block'}}
                                           />)
                                        : (this.state.previewItem !== null && this.getType(this.state.previewItem.file_extension) === 'PDF' && this.state.fileUrl !== ''
                                            ?  (<div align={'center'} style={{marginTop: 20}}>
                                                    <Document
                                                        file={this.state.fileUrl}
                                                        onLoadSuccess={this.onDocumentLoadSuccess}
                                                        loading={""}
                                                        options={this.options}
                                                    >
                                                    {<Page
                                                        pageNumber={this.state.numPages === 1 ? this.state.pageNumber : this.state.pageNumber || 1}
                                                        onLoadSuccess={(page) => this.setState({
                                                            pageWidth: page.originalWidth,
                                                            pageHeight: page.originalHeight
                                                        })}
                                                        scale={this.setPageScale()}
                                                    />
                                                    }
                                                    { this.state.numPages > 1
                                                        ? (<div>
                                                            <button className="page-button"
                                                                    type="button"
                                                                    disabled={this.state.pageNumber === 1}
                                                                    onClick={this.previousPage}>
                                                                Previous
                                                            </button>
                                                            <button className="page-button"
                                                                    type="button"
                                                                    disabled={this.state.pageNumber >= this.state.numPages}
                                                                    onClick={this.nextPage}>
                                                                Next
                                                            </button>
                                                        </div>)
                                                        : null
                                                    }
                                                </Document>
                                            </div>)
                                            : null)
                                    )
                                }
                            </Modal>

                            <Drawer
                                width={400}
                                title="File Details"
                                placement="right"
                                onClose={this.onDetailsClose}
                                visible={this.state.isDetailsVisible}
                            >
                                {this.state.detailsItem ?
                                    (<React.Fragment>
                                        <h2>{this.state.detailsItem.file_name}.{this.state.detailsItem.file_extension}</h2>
                                        <Card
                                            style={{ width: '100%', marginBottom: 20}}
                                            bodyStyle={{ padding: 0 }}
                                            cover={
                                                <React.Fragment>
                                                    <Avatar
                                                        icon={this.getAvatar(this.state.detailsItem.file_extension)}
                                                        style={{fontSize: 150, height: '120%', paddingTop: 10, paddingBottom: 10}}
                                                        shape={'square'}
                                                    />
                                                </React.Fragment>
                                            }
                                        >
                                        </Card>
                                        <Row>
                                            <Col span={5}>
                                                <p>Type:</p>
                                            </Col>
                                            <Col span={19}>
                                                <p>{this.getType(this.state.detailsItem.file_extension)}</p>
                                            </Col>

                                        </Row>
                                        <Row>
                                            <Col span={5}>
                                                <p>Size:</p>
                                            </Col>
                                            <Col span={19}>
                                                <p>{this.formatBytes(this.state.detailsItem.file_size)} ({this.formatBytesWithCommas(this.state.detailsItem.file_size)} bytes)</p>
                                            </Col>

                                        </Row>
                                        <Row>
                                            <Col span={5}>
                                                <p>Modified:</p>
                                            </Col>
                                            <Col span={19}>
                                                <p>{this.parseDate(this.state.detailsItem.updated_at)}</p>
                                            </Col>

                                        </Row>
                                        <Row>
                                            <Col span={5}>
                                                <p>Created:</p>
                                            </Col>
                                            <Col span={19}>
                                                <p>{this.parseDate(this.state.detailsItem.created_at)}</p>
                                            </Col>
                                        </Row>
                                    </React.Fragment>)
                                    :
                                    null
                                }
                            </Drawer>
                        </Layout.Content>
                    </Layout>
                </Layout>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    items: state.files.allFiles,
    folders: [{id:1},{id:2},{id:3},{id:4},{id:5},{id:6},{id:7},{id:8}],
    currentUploadingFileName: state.files.currentUploadingFileName,
    isFileUploading: state.files.isFileUploading,
    fileUploadProgress: state.files.fileUploadProgress,
    hadUploadError: state.files.hadUploadError,
    currentDownloadingFileName: state.files.currentDownloadingFileName,
    isFileDownloading: state.files.isFileDownloading,
    fileDownloadProgress: state.files.fileDownloadProgress,
    isFetchingAllFiles: state.files.isFetchingAllFiles,
    fetchedAllFiles: state.files.fetchedAllFiles,
    user: state.auth.user,
    storage: state.auth.storage,
    fileDataBlob: state.files.fileDataBlob,
    fetchingFileData: state.files.fetchingFileData,
    filteredAllFiles: state.filteredAllFiles,
    filteredStarredFiles: state.filteredStarredFiles,
    filteredTrashedFiles: state.filteredTrashedFiles,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
      logoutUser, updateStorage, fetchAllFiles,
      fetchStarredFiles, fetchTrashedFiles,
      uploadNewFile, updateFile, downloadFile, getFileData,
      duplicateFile, renameFile
  }, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UserDashboard)
)
