import UserDashboard from './UserDashboard'
import UserStarred from "./UserStarred";
import UserSettings from "./UserSettings";
import UserTrash from "./UserTrash";

export { UserDashboard, UserStarred, UserSettings, UserTrash }
