import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Layout, Card, notification, Progress, Form, Input, Button, Col, Row } from "antd";
import { updateStorage, logoutUser, updateUser } from '../../../actions/authActions'
import { uploadNewFile, fetchAllFiles} from '../../../actions/fileActions'
import { Sidebar, UserHeader } from "../../../components";
import { UserOutlined } from "@ant-design/icons";
import './style.css'

class UserSettings extends Component {
    state = {
        width: window.innerWidth,
        file: null,
        percentage: 0,
        option: 'normal',
        name: '',
        downloadPercentage: 0,
        downloadOption: 'normal',
        downloadName: '',
        form: {
            first_name: this.props.user.basic_user.data.first_name,
            last_name: this.props.user.basic_user.data.last_name
        }
    };

    onFinish = values => {
        this.setState({
            form: {
                first_name: values.first_name,
                last_name: values.last_name
            }
        });
        const { updateUser } = this.props
        updateUser({user_id: this.props.user.id, basic_user_id: this.props.user.basic_user.data.id, form: this.state.form})
    }

    triggerSidebarUpload = (file) => {
        const { user } = this.props
        this.setState({
            file: file,
            name: file.name,
        });

        const formData = new FormData()
        formData.append('file', file)
        const data = {
            owner_id: user.id,
        }
        formData.set('data', JSON.stringify(data))
        this.props.uploadNewFile({formData: formData, user_id: user.id, name: file.name}).then(()=>{
            if(!this.props.hadUploadError) {
                this.props.fetchAllFiles({user_id: this.props.user.id})
                this.props.updateStorage({user_id: user.id, basic_user_id: user.basic_user.data.id})
            }
        })
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
        if (this.props.hadUploadError === false && nextProps.hadUploadError === true) {
            this.setState({
                option: 'exception',
                percentage: 0,
                name: nextProps.currentUploadingFileName
            }, ()=> {
                this.openNotification()
            })
        }
        else if (nextProps.isFileUploading) {
            this.openNotification()
            if (nextProps.fileUploadProgress < 100) {
                this.setState({
                    option: 'normal',
                    percentage: nextProps.fileUploadProgress.toFixed(0),
                    name: nextProps.currentUploadingFileName
                }, ()=> {
                    this.openNotification()
                })
            }
            if (nextProps.fileUploadProgress === 100) {
                this.setState({
                    option: 'success',
                    percentage: 100,
                    name: nextProps.currentUploadingFileName
                }, ()=> {
                    this.openNotification()
                })
            }
        }

        if (nextProps.isFileDownloading) {
            this.openDownloadNotification()
            if (nextProps.fileDownloadProgress < 100) {
                this.setState({
                    downloadOption: 'normal',
                    downloadPercentage: nextProps.fileDownloadProgress.toFixed(0),
                    downloadName: nextProps.currentDownloadingFileName
                }, ()=> {
                    this.openDownloadNotification()
                })
            }
            if (nextProps.fileDownloadProgress === 100) {
                this.setState({
                    downloadOption: 'success',
                    downloadPercentage: 100,
                    downloadName: nextProps.currentDownloadingFileName
                }, ()=> {
                    this.openDownloadNotification()
                })
            }
        }
    }

    openNotification = () => {
        notification.open({
            key: 'uploadKey',
            message: 'Uploading',
            description:
                <React.Fragment>
                    <Progress
                        type={'circle'}
                        strokeColor={{
                            '0%': '#108ee9',
                            '100%': '#87d068',
                        }}
                        status={this.state.option}
                        percent={this.state.percentage}
                        width={50}
                        style={{marginRight: 10}}
                    />
                    {this.state.name}
                </React.Fragment>,
            placement: 'bottomRight',
            duration: 0,
        });
    };

    openDownloadNotification = () => {
        notification.open({
            key: 'downloadKey',
            message: 'Downloading',
            description:
                <React.Fragment>
                    <Progress
                        type={'circle'}
                        strokeColor={{
                            '0%': '#108ee9',
                            '100%': '#87d068',
                        }}
                        status={this.state.downloadOption}
                        percent={this.state.downloadPercentage}
                        width={50}
                        style={{marginRight: 10}}
                    />
                    {this.state.downloadName}
                </React.Fragment>,
            placement: 'bottomRight',
            duration: 0
        });
    };

    componentDidMount() {
        window.addEventListener("resize", this.handleResize);
    }

    handleResize = e => {
        const windowSize = window.innerWidth;
        this.setState({
            width: windowSize
        })
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize);
    }

    render() {
        const { storage } = this.props
        return (
            <Layout>
                <UserHeader
                    history={this.props.history}
                    onLogout={this.props.logoutUser}
                    filesForSearch={[]}
                    page={"settings"}
                />
                <Layout>
                    <Sidebar
                        history={this.props.history}
                        storage={this.props.storage}
                        triggerSidebarUpload={this.triggerSidebarUpload}
                    />
                    <Layout>
                        <Layout.Content>
                            <Card style={{height: 'calc(100vh - 65px)', overflow: 'auto'}} bodyStyle={{padding: "0"}}>
                                <div style={{fontSize: 18, marginTop: 16, paddingBottom: 16, borderBottom: '1px solid rgb(88, 88, 88)'}}>
                                    <div style={{marginLeft: 20, color: 'white'}}>
                                        Settings
                                    </div>
                                </div>
                                <Row gutter={20} style={{marginRight: 40, marginLeft: 10, marginTop: 15}}>
                                    <Col xs={{span:24}} sm={{span:24}} md={{span:11}} lg={{span:10}} xl={{span:8}} xxl={{span:6}} style={{marginBottom: 15}}>
                                        <p className={'user-settings-section-header'}>
                                            Update user:
                                        </p>
                                        <Form
                                            name="User settings"
                                            onFinish={this.onFinish}
                                        >
                                            <Form.Item
                                                name="first_name"
                                                rules={[{ required: true, message: 'Please enter your First Name!' },
                                                    {type: 'string', min: 2, message: 'First name too short!'},
                                                    {type: 'string', max: 100, message: 'First name too long!'}]}
                                                validateTrigger={'onSubmit'}
                                                initialValue={this.state.form.first_name}
                                            >
                                                <Input
                                                    prefix={<UserOutlined className="site-form-item-icon" />}
                                                    type={'string'}
                                                    placeholder="First Name"
                                                />
                                            </Form.Item>
                                            <Form.Item
                                                name="last_name"
                                                rules={[{ required: true, message: 'Please enter your Last Name!' },
                                                    {type: 'string', min: 2, message: 'Last name too short!'},
                                                    {type: 'string', max: 100, message: 'Last name too long!'}]}
                                                validateTrigger={'onSubmit'}
                                                initialValue={this.state.form.last_name}
                                            >
                                                <Input
                                                    prefix={<UserOutlined className="site-form-item-icon" />}
                                                    type={'string'}
                                                    placeholder="Last Name"
                                                />
                                            </Form.Item>
                                            <Form.Item>
                                                <div align={'right'}>
                                                    <Button type="primary" htmlType="submit" loading={this.props.isUpdatingUser}>
                                                        Update
                                                    </Button>
                                                </div>
                                            </Form.Item>
                                        </Form>
                                        <p className="user-settings-progress-section">
                                            Remaining storage space:
                                        </p>
                                        <div style={{marginTop: 15, marginLeft: 25, marginRight: 12}}>
                                            <Progress type="circle"
                                                      percent={((storage/1048576)/1024)*100}
                                                      format={percent => `${parseFloat((percent/100).toFixed(3))}/1GB`}                                                      size="small"
                                                      width={this.state.width > 450 ? 300 : this.state.width-175}
                                            />
                                        </div>
                                    </Col>
                                    <Col xs={{span:0}} sm={{span:0}} md={{span:1}} lg={{span:2}} xl={{span:2}} xxl={{span:2}}>

                                    </Col>
                                    <Col xs={{span:24}} sm={{span:24}} md={{span:11}} lg={{span:10}} xl={{span:8}} xxl={{span:6}}>

                                    </Col>
                                </Row>
                            </Card>
                        </Layout.Content>
                    </Layout>
                </Layout>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
    isUpdatingUser: state.auth.isUpdatingUser,
    currentUploadingFileName: state.files.currentUploadingFileName,
    isFileUploading: state.files.isFileUploading,
    fileUploadProgress: state.files.fileUploadProgress,
    hadUploadError: state.files.hadUploadError,
    currentDownloadingFileName: state.files.currentDownloadingFileName,
    isFileDownloading: state.files.isFileDownloading,
    fileDownloadProgress: state.files.fileDownloadProgress,
    storage: state.auth.storage
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({logoutUser, uploadNewFile, fetchAllFiles, updateStorage, updateUser}, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UserSettings)
)
