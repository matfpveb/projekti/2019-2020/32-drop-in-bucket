import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {Layout, Button} from "antd";
import { logoutUser } from '../../../actions/authActions'


class AdminDashboard extends Component {
    state = {
        margin: 200,
    };

    onLogout = () => {
        this.props.logoutUser();
    };

    render() {
        return (
            <Layout.Content>
                <div>Admin</div>
                <Button onClick={this.onLogout}>
                    Logout
                </Button>
            </Layout.Content>
        )
    }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch =>
  bindActionCreators({logoutUser}, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AdminDashboard)
)
