import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {Row, Col, Card, Button, Form, Input} from 'antd'

import { registerUser } from '../../actions/authActions'
import { Link } from '../../components'
import {LockOutlined, UserOutlined, MailOutlined} from "@ant-design/icons";
import './style.css'
import tile from "../../assets/images/bucket_tile.png"

class RegisterPage extends Component {
    static propTypes = {
        registering: PropTypes.bool,
        registerUser: PropTypes.func
    }

    state = {
        form: {
            email: '',
            password: '',
            first_name: '',
            last_name: '',
        }
    }

    onFinish = values => {
        this.setState({
            form: {
                email: values.email,
                password: values.password,
                first_name: values.first_name,
                last_name: values.last_name
            }
        });
        const { registerUser } = this.props;
        registerUser({...this.state.form})
    };

    render() {
        const { isRegistering, history } = this.props

        return (
            <Row className={'registerRow'} style={{ height: '100%', background: `url(${tile})` }} type="flex" justify="center">
                <Col className="col1" span={24}>
                    <Card style={{ top: '50%', transform: 'translateY(-50%)' }}>
                        <Form
                            name="register"
                            className="register-form"
                            onFinish={this.onFinish}
                        >
                            <Form.Item
                                name="email"
                                rules={[{type: 'email', message: 'The input is not valid email!'},{ required: true, message: 'Please input your Email!' }]}
                                validateTrigger={'onSubmit'}
                            >
                                <Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[{ required: true, message: 'Please enter a Password!' },
                                    {type: 'string', min: 6, message: 'Password must be 6 characters or longer!'},
                                    {type: 'string', max: 100, message: 'Password too long!'}]}
                                validateTrigger={'onSubmit'}
                            >
                                <Input
                                    prefix={<LockOutlined className="site-form-item-icon" />}
                                    type="password"
                                    placeholder="Password"
                                />
                            </Form.Item>
                            <Form.Item
                                name="first_name"
                                rules={[{ required: true, message: 'Please enter your First Name!' },
                                    {type: 'string', min: 2, message: 'First name too short!'},
                                    {type: 'string', max: 100, message: 'First name too long!'}]}
                                validateTrigger={'onSubmit'}
                            >
                                <Input
                                    prefix={<UserOutlined className="site-form-item-icon" />}
                                    type={'string'}
                                    placeholder="First Name"
                                />
                            </Form.Item>
                            <Form.Item
                                name="last_name"
                                rules={[{ required: true, message: 'Please enter your Last Name!' },
                                    {type: 'string', min: 2, message: 'Last name too short!'},
                                    {type: 'string', max: 100, message: 'Last name too long!'}]}
                                validateTrigger={'onSubmit'}
                            >
                                <Input
                                    prefix={<UserOutlined className="site-form-item-icon" />}
                                    type={'string'}
                                    placeholder="Last Name"
                                />
                            </Form.Item>

                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="register-form-button"  loading={isRegistering}>
                                    Register
                                </Button>
                                <div className={"login-link"}>
                                    <Link to="/login" history={history}>
                                        Already have an account? Click here to Sign in!
                                    </Link>
                                </div>
                            </Form.Item>
                        </Form>

                    </Card>
                </Col>
            </Row>
        )
    }
}

const mapStateToProps = state => ({
    redirect: state.auth.redirect,
    isRegistering: state.auth.registering,
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({ registerUser }, dispatch)

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(RegisterPage)
)
