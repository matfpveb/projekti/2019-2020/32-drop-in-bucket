import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Row, Col, Card, Button, Form, Input, Checkbox } from 'antd'
import { bindActionCreators } from 'redux'
import { loginUser } from '../../actions/authActions'
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import './style.css'
import { Link } from "../../components";
import tile from "../../assets/images/bucket_tile.png"
class LoginPage extends Component {
  static propTypes = {
    loginUser: PropTypes.func,
    isLoggingIn: PropTypes.bool,
  };

  state = {
    form: {
      email: '',
      password: '',
    },
  };

  onFinish = values => {
    this.setState({
      form: {
        email: values.email,
        password: values.password
      }
    });
    const { loginUser } = this.props;
    loginUser({form: this.state.form, redirectTo: this.props.redirect})
  };

  render() {
    const { isLoggingIn, history } = this.props;
    return (
        <Row className={'loginRow'} style={{ height: '100%', background: `url(${tile})` }} type="flex" justify="center">
          <Col className="col1" span={24}>
            <Card style={{ top: '50%', transform: 'translateY(-50%)' }}>
              <Form
                  name="login"
                  className="login-form"
                  initialValues={{ remember: true }}
                  onFinish={this.onFinish}
              >
                <Form.Item
                    name="email"
                    rules={[{type: 'email', message: 'The input is not valid email!'},{ required: true, message: 'Please input your Email!' }]}
                    validateTrigger={'onSubmit'}
                >
                  <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your Password!' }]}
                    validateTrigger={'onSubmit'}
                >
                  <Input
                      prefix={<LockOutlined className="site-form-item-icon" />}
                      type="password"
                      placeholder="Password"
                  />
                </Form.Item>
                <Form.Item>
                  <Form.Item name="remember" valuePropName="checked" noStyle>
                    <Checkbox>Remember me</Checkbox>
                  </Form.Item>
                </Form.Item>

                <Form.Item>
                  <Button type="primary" htmlType="submit" className="login-form-button"  loading={isLoggingIn}>
                    Log in
                  </Button>
                  <div className={"register-link"}>
                    <Link to="/register" history={history}>
                      Don't have an account? Sign up!
                    </Link>
                  </div>
                </Form.Item>
              </Form>
            </Card>
          </Col>
        </Row>
    )
  }
}

const mapStateToProps = state => ({
  redirect: state.auth.redirect,
  isLoggingIn: state.auth.loggingIn,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({loginUser}, dispatch);

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(LoginPage)
)
