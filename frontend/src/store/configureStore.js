import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk'

import rootReducer from '../reducers/rootReducer'

const history = createBrowserHistory();
const middleware = routerMiddleware(history);

const configureStore = () => {
  return createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunk, middleware)
  )
}

export { configureStore, history }
