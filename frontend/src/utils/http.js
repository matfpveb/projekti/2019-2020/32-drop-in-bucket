import axios from 'axios'

import storage from '../utils/storage'

const getDefaultRequestOptions = () => {
  const authData = storage.retrieveAuthData();
  const accessToken = authData ? authData.access_token : null;

  return {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`,
    },
  }
};

const httpGet = (
  url,
  payload = null,
  requestOptions = getDefaultRequestOptions()
) => axios.get(`${url}${createQueryString(payload)}`, requestOptions);

const httpPost = (url, payload, requestOptions = getDefaultRequestOptions()) =>
  axios.post(url, JSON.stringify(payload), requestOptions);

const httpPut = (url, payload, requestOptions = getDefaultRequestOptions()) =>
  axios.put(url, JSON.stringify(payload), requestOptions);

const httpDelete = (url, requestOptions = getDefaultRequestOptions()) =>
  axios.delete(url, requestOptions);

const createQueryString = payload => {
  if (payload === null || Object.keys(payload).length === 0) {
    return ''
  }

  const esc = encodeURIComponent;

  let query = '?';

  query += Object.keys(payload)
    .map(key => esc(key) + '=' + esc(payload[key]))
    .join('&');

  return query
};

const getErrorMessage = error => error.response.data.message;

export default {
  get: httpGet,
  post: httpPost,
  put: httpPut,
  delete: httpDelete,
  getErrorMessage,
}
