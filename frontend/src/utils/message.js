import { message } from 'antd'

const type = {
  success: 'success',
  error: 'error',
  info: 'info',
  warning: 'warning',
};

const show = (type, content) => {
  message[type](content)
};

export default {
  show,
  type,
}
