const storeAuthData = data => {
  localStorage.setItem('drop_in_bucket_auth_data', JSON.stringify(data))
};

const retrieveAuthData = () =>
  JSON.parse(localStorage.getItem('drop_in_bucket_auth_data'));

const updateUser = data => {
  localStorage.setItem(
    'drop_in_bucket_auth_data',
    JSON.stringify({
      ...JSON.parse(localStorage.getItem('drop_in_bucket_auth_data')),
      user: {data: data},
    })
  )
};

export default {
  storeAuthData,
  retrieveAuthData,
  updateUser,
}
