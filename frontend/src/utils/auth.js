let store = null;

const user = () => {
  return store ? store.getState().auth.user : null
};

const setStore = newStore => {
  store = newStore
};

export default {
  user,
  setStore,
}
