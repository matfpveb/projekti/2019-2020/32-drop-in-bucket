import React from 'react'
import { Redirect } from 'react-router-dom'

import {
  LoginPage,
  RegisterPage
} from '../containers'

import { AdminDashboard } from '../containers/admin'

import {
  UserDashboard,
  UserStarred,
  UserSettings,
  UserTrash
} from '../containers/user'

import { withRedirect } from './helpers'

const withMiddleware = ({ component: Component, roleNames }) => () => {
  if (store.getState().auth.user === null || store.getState().auth.user === undefined) {
    return <Redirect to={{ pathname: '/login' }} />
  }
  const roleName = store.getState().auth.user.authorities.data[0].name;

  if (!roleNames.includes(roleName)) {
    return <Redirect to={{ pathname: '/' }} />
  }

  return <Component />
};

let store;

const routes = {
  all: [
    {
      path: '/',
      exact: true,
      component: () => {
        if (store.getState().auth.user === null || store.getState().auth.user === undefined) {
          return <Redirect to={{ pathname: '/login' }} />
        }


        const roleName = store.getState().auth.user.authorities.data[0].name;
        switch (roleName) {
          case 'admin':
            return <AdminDashboard />;

          case 'basic_user':
            return <UserDashboard />;

          default:
            return null
        }
      },
    },
    {
      path: '/starred',
      exact: true,
      component: withMiddleware({
        component: UserStarred,
        roleNames: ['basic_user'],
      }),
    },
    {
      path: '/trash',
      exact: true,
      component: withMiddleware({
        component: UserTrash,
        roleNames: ['basic_user'],
      }),
    },
    {
      path: '/user-settings',
      exact: true,
      component: withMiddleware({
        component: UserSettings,
        roleNames: ['basic_user'],
      }),
    },
    {
      path: '/login',
      exact: true,
      isPublic: true,
      component: withRedirect({
        ifTrue: () => store.getState().auth.user === null,
        thenRenderComponent: LoginPage,
        elseRedirectTo: '/',
      }),
    },
    {
      path: '/register',
      exact: true,
      isPublic: true,
      component: withRedirect({
        ifTrue: () => store.getState().auth.user === null,
        thenRenderComponent: RegisterPage,
        elseRedirectTo: '/',
      }),
    },
    {
      path: '/*',
      isPublic: true,
      component: () => <Redirect to={{ pathname: '/login' }} />,
    },
  ],
  setStore: newStore => {
    store = newStore
  },
};

export { routes }
