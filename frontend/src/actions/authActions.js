import {
    AUTH_SET_USER,
    AUTH_UPDATE_USER,
    AUTH_UPDATING_USER,
    AUTH_SET_LOGGING_IN,
    AUTH_SET_REGISTERING,
    AUTH_SET_STORAGE
} from './actionTypes'

import { push } from 'react-router-redux'

import { API_BASE_URL } from '../constants'

import { http, storage, message } from '../utils'

const registerUser = payload => dispatch => {
    dispatch({
        type: AUTH_SET_REGISTERING,
        payload: true,
    });

    return http
        .post(`${API_BASE_URL}/basicUser`, payload)
        .then(() => {
            dispatch({ type: AUTH_SET_REGISTERING, payload: false });
            dispatch(push('/login'));
            message.show(
                message.type.success,
                'Successfully registered'
            )
        })
        .catch(error => {
            message.show(message.type.error, http.getErrorMessage(error));
            dispatch({ type: AUTH_SET_REGISTERING, payload: false })
        })
};

const loginUser = payload => dispatch => {
    dispatch({
        type: AUTH_SET_LOGGING_IN,
        payload: true,
    })

    return http
        .post(`${API_BASE_URL}/auth/login`, {...payload.form})
        .then(response => {
            const authData = response.data.data;
            storage.storeAuthData(authData);
            dispatch({
                type: AUTH_SET_USER,
                payload: authData.user.data,
            });
            if (authData.user.data.basic_user) {
                dispatch({
                    type: AUTH_SET_STORAGE,
                    payload: authData.user.data.basic_user.data.storage_used,
                })
            }
            dispatch({
                type: AUTH_SET_LOGGING_IN,
                payload: false,
            });

            dispatch(push(payload.redirectTo || '/'))
        })
        .catch(error => {
            if(error.response !== undefined) {
                message.show(message.type.error, http.getErrorMessage(error))
            }
            else {
                message.show(message.type.error, "Server could not be reached")
            }

            dispatch({
                type: AUTH_SET_LOGGING_IN,
                payload: false,
            })
        })
};

const updateUser = payload => dispatch => {
    dispatch({
        type: AUTH_UPDATING_USER,
        payload: true
    })
    return http
        .put(`${API_BASE_URL}/users/${payload.user_id}/basic_users/${payload.basic_user_id}`, {...payload.form})
        .then(response => {
            const userData = response.data.data;
            storage.updateUser(userData);
            dispatch({
                type: AUTH_UPDATE_USER,
                payload: userData,
            });
            dispatch({
                type: AUTH_UPDATING_USER,
                payload: false,
            })
            message.show(message.type.success, "Successfully updated user")
        })
        .catch(error => {
            if(error.response !== undefined) {
                message.show(message.type.error, http.getErrorMessage(error))
            }
            else {
                message.show(message.type.error, "Server could not be reached")
            }
            dispatch({
                type: AUTH_UPDATING_USER,
                payload: false,
            })
        })
}

const updateStorage = payload => dispatch => {

    http
        .get(`${API_BASE_URL}/users/${payload.user_id}/basic_users/${payload.basic_user_id}`)
        .then(response => {
            const rez = response.data.data.basic_user.data
            storage.updateUser(response.data.data)
            dispatch({
                type: AUTH_SET_STORAGE,
                payload: rez.storage_used,
            })

        })
        .catch(error =>{})
};

const logoutUser = () => dispatch => {
    const authData = storage.retrieveAuthData()
    if(authData !== null && authData.user.data !== undefined)
    {
        http
            .post(`${API_BASE_URL}/users/${authData.user.data.id}/logout`)
            .catch(error =>{})
    }
    storage.storeAuthData(null)
    dispatch({
        type: AUTH_SET_USER,
        payload: null,
    })
    message.show(message.type.info, 'You have been successfully logged out.')
    dispatch(push('/login'))
};

export { loginUser, registerUser, updateStorage, logoutUser, updateUser}