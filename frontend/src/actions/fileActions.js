import {
    FILES_SET_FETCHING_ALL_FILES,
    FILES_SET_ALL_FILES,
    FILES_SET_FETCHING_STARRED_FILES,
    FILES_SET_STARRED_FILES,
    FILES_SET_FETCHING_TRASHED_FILES,
    FILES_SET_TRASHED_FILES,
    FILES_SET_FILE_UPLOADING,
    FILES_SET_FILE_UPLOAD_PROGRESS,
    FILES_SET_FETCHED_ALL_FILES,
    FILES_SET_FETCHED_STARRED_FILES,
    FILES_SET_FETCHED_TRASHED_FILES,
    FILES_SET_FILE_UPLOADING_ERROR,
    FILES_TRASH_FILE,
    FILES_STARRED_FILE,
    FILES_BASIC_FROM_STARRED_FILE,
    FILES_BASIC_FROM_TRASHED_FILE,
    FILES_DELETE_FILE,
    FILES_SET_FILE_DOWNLOADING,
    FILES_SET_FILE_DOWNLOAD_PROGRESS,
    FILES_SET_GETTING_FILE_DATA,
    FILES_FILE_DATA,
    FILES_SET_NEW_UPLOAD_FILE_NAME,
    FILES_SET_NEW_DOWNLOAD_FILE_NAME,
    FILES_RENAME_STARRED_FILE,
    FILES_RENAME_BASIC_FILE,
    FILES_UPDATE_MODIFICATION_TIME_BASIC,
    FILES_UPDATE_MODIFICATION_TIME_STARRED,
    FILES_UPDATE_MODIFICATION_TIME_TRASHED
} from "./actionTypes";
import {http, message} from "../utils";
import {API_BASE_URL} from "../constants";
import axios from 'axios'
import FileSaver from 'file-saver'
import storage from "../utils/storage";

const fetchAllFiles = payload => dispatch => {
    dispatch({
        type: FILES_SET_FETCHING_ALL_FILES,
        payload: true,
    });

    return http
        .get(`${API_BASE_URL}/users/${payload.user_id}/files/basic`)
        .then(response => {
            const allFiles = response.data.files ? response.data.files : response.data.data;
            if (allFiles.length !== 0) {
                allFiles.sort(function(a, b){
                    if (a.id < b.id) {
                        return 1;
                    }
                    if (a.id > b.id) {
                        return -1;
                    }
                    return 0;
                })
            }
            dispatch({
                type: FILES_SET_ALL_FILES,
                payload: allFiles,
            });

            dispatch({
                type: FILES_SET_FETCHING_ALL_FILES,
                payload: false,
            });

            dispatch({
                type: FILES_SET_FETCHED_ALL_FILES,
                payload: true,
            });
        })
        .catch(error => {
            if(error.response !== undefined) {
                message.show(message.type.error, http.getErrorMessage(error))
            }
            else {
                message.show(message.type.error, "Server could not be reached")
            }

            dispatch({
                type: FILES_SET_FETCHING_ALL_FILES,
                payload: false,
            });
        })
};

const fetchStarredFiles = payload => dispatch => {
    dispatch({
        type: FILES_SET_FETCHING_STARRED_FILES,
        payload: true,
    })

    return http
        .get(`${API_BASE_URL}/users/${payload.user_id}/files/starred`)
        .then(response => {
            const starredFiles = response.data.files ? response.data.files : response.data.data;
            if (starredFiles.length !== 0) {
                starredFiles.sort(function(a, b){
                    if (a.id < b.id) {
                        return 1;
                    }
                    if (a.id > b.id) {
                        return -1;
                    }
                    return 0;
                })
            }
            dispatch({
                type: FILES_SET_STARRED_FILES,
                payload: starredFiles,
            });

            dispatch({
                type: FILES_SET_FETCHING_STARRED_FILES,
                payload: false,
            });

            dispatch({
                type: FILES_SET_FETCHED_STARRED_FILES,
                payload: true,
            });
        })
        .catch(error => {
            if(error.response !== undefined) {
                message.show(message.type.error, http.getErrorMessage(error))
            }
            else {
                message.show(message.type.error, "Server could not be reached")
            }

            dispatch({
                type: FILES_SET_FETCHING_STARRED_FILES,
                payload: false,
            })
        })
};

const fetchTrashedFiles = payload => dispatch => {
    dispatch({
        type: FILES_SET_FETCHING_TRASHED_FILES,
        payload: true,
    })

    return http
        .get(`${API_BASE_URL}/users/${payload.user_id}/files/trashed`)
        .then(response => {
            const trashedFiles = response.data.files ? response.data.files : response.data.data;
            if (trashedFiles.length !== 0) {
                trashedFiles.sort(function(a, b){
                    if (a.id < b.id) {
                        return 1;
                    }
                    if (a.id > b.id) {
                        return -1;
                    }
                    return 0;
                })
            }
            dispatch({
                type: FILES_SET_TRASHED_FILES,
                payload: trashedFiles,
            });

            dispatch({
                type: FILES_SET_FETCHING_TRASHED_FILES,
                payload: false,
            });

            dispatch({
                type: FILES_SET_FETCHED_TRASHED_FILES,
                payload: true,
            });
        })
        .catch(error => {
            if(error.response !== undefined) {
                message.show(message.type.error, http.getErrorMessage(error))
            }
            else {
                message.show(message.type.error, "Server could not be reached")
            }

            dispatch({
                type: FILES_SET_FETCHING_TRASHED_FILES,
                payload: false,
            });
        })
};

const uploadNewFile = payload => dispatch => {
    dispatch({
        type: FILES_SET_NEW_UPLOAD_FILE_NAME,
        payload: payload.name,
    });

    dispatch({
        type: FILES_SET_FILE_UPLOADING,
        payload: true,
    });

    dispatch({
        type: FILES_SET_FILE_UPLOADING_ERROR,
        payload: false,
    });

    dispatch({
        type: FILES_SET_FILE_UPLOAD_PROGRESS,
        payload: 0,
    });

    const authData = storage.retrieveAuthData()
    const accessToken = authData ? authData.access_token : null


    return axios({
        method: 'post',
        url: `${API_BASE_URL}/users/${payload.user_id}/files`,
        data: payload.formData,
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
        onUploadProgress (progressEvent) {
            let progress = parseInt(((progressEvent.loaded / progressEvent.total) * 100).toFixed(0))
            dispatch({
                type: FILES_SET_FILE_UPLOAD_PROGRESS,
                payload: progress,
            })
        }
        })
        .then(response => {
            dispatch({
                type: FILES_SET_FILE_UPLOAD_PROGRESS,
                payload: 100,
            })
            dispatch({
                type: FILES_SET_FILE_UPLOADING,
                payload: false,
            });
        })
        .catch(error => {
            dispatch({
                type: FILES_SET_FILE_UPLOADING_ERROR,
                payload: true,
            });
            message.show(message.type.error, http.getErrorMessage(error))
        })
};

const updateFile = payload => dispatch => {
    let tmp_item = payload.item
    if(payload.state === 'trashed') {
        tmp_item.state = 'trashed'
        dispatch({
            type: FILES_TRASH_FILE,
            payload: tmp_item,
        });
    }
    else if(payload.state === 'starred') {
        tmp_item.state = 'starred'
        dispatch({
            type: FILES_STARRED_FILE,
            payload: tmp_item,
        });
    }
    else {
        if (tmp_item.state === 'starred') {
            tmp_item.state = 'basic'
            dispatch({
                type: FILES_BASIC_FROM_STARRED_FILE,
                payload: tmp_item,
            });
        } else {
            tmp_item.state = 'basic'
            dispatch({
                type: FILES_BASIC_FROM_TRASHED_FILE,
                payload: tmp_item,
            });
        }

    }
    return http
        .put(`${API_BASE_URL}/users/${payload.user_id}/files/${payload.file_id}`, {name: payload.name, state: payload.state})
        .then(response => {
            const rez = response.data.data
            if (rez.state === 'basic') {
                dispatch({
                    type: FILES_UPDATE_MODIFICATION_TIME_BASIC,
                    payload: rez,
                });
            }
            else if (rez.state === 'starred') {
                dispatch({
                    type: FILES_UPDATE_MODIFICATION_TIME_STARRED,
                    payload: rez,
                });
            }
            else if (rez.state === 'trashed') {
                dispatch({
                    type: FILES_UPDATE_MODIFICATION_TIME_TRASHED,
                    payload: rez,
                });
            }
        })
        .catch(error => {
            if(error.response !== undefined) {
                message.show(message.type.error, http.getErrorMessage(error))
            }
            else {
                message.show(message.type.error, "Server could not be reached")
            }
        })
};

const renameFile = payload => dispatch => {
    let tmp_item = payload.item

    if(tmp_item.state === 'starred') {
        dispatch({
            type: FILES_RENAME_STARRED_FILE,
            payload: tmp_item,
        });
    }
    else if(tmp_item.state === 'basic') {
        dispatch({
            type: FILES_RENAME_BASIC_FILE,
            payload: tmp_item,
        });
    }

    return http
        .put(`${API_BASE_URL}/users/${payload.user_id}/files/${payload.file_id}`, {name: tmp_item.file_name, state: tmp_item.state})
        .then( response => {
            const rez = response.data.data
            if (rez.state === 'basic') {
                dispatch({
                    type: FILES_UPDATE_MODIFICATION_TIME_BASIC,
                    payload: rez,
                });
            }
            else if (rez.state === 'starred') {
                dispatch({
                    type: FILES_UPDATE_MODIFICATION_TIME_STARRED,
                    payload: rez,
                });
            }
            else if (rez.state === 'trashed') {
                dispatch({
                    type: FILES_UPDATE_MODIFICATION_TIME_TRASHED,
                    payload: rez,
                });
            }
        })
        .catch(error => {
            if(error.response !== undefined) {
                message.show(message.type.error, http.getErrorMessage(error))
            }
            else {
                message.show(message.type.error, "Server could not be reached")
            }
        })
};

const downloadFile = payload => dispatch => {
    dispatch({
        type: FILES_SET_NEW_DOWNLOAD_FILE_NAME,
        payload: payload.name,
    });

    dispatch({
        type: FILES_SET_FILE_DOWNLOADING,
        payload: true,
    });

    dispatch({
        type: FILES_SET_FILE_DOWNLOAD_PROGRESS,
        payload: 0,
    });

    const authData = storage.retrieveAuthData()
    const accessToken = authData ? authData.access_token : null;
    axios({
        method: 'get',
        url: `${API_BASE_URL}/users/${payload.user_id}/files/${payload.file_id}`,
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
        onDownloadProgress (progressEvent) {
            let progress = parseInt(((progressEvent.loaded / payload.item.file_size) * 100).toFixed(0))
            dispatch({
                type: FILES_SET_FILE_DOWNLOAD_PROGRESS,
                payload: progress,
            })
        },
        responseType: 'arraybuffer'
    }).then(response => {
        dispatch({
            type: FILES_SET_FILE_DOWNLOAD_PROGRESS,
            payload: 100,
        })
        dispatch({
            type: FILES_SET_FILE_DOWNLOADING,
            payload: false,
        });
        let blob = new Blob([response.data])
        const file = new File([blob], `${payload.item.file_name}.${payload.item.file_extension}`)
        FileSaver.saveAs(file)
    }).catch( error => {
        message.show(message.type.error, 'File does not exist')
    })
}

const getFileData = payload => dispatch => {
    dispatch({
        type: FILES_SET_GETTING_FILE_DATA,
        payload: true,
    });

    const authData = storage.retrieveAuthData()
    const accessToken = authData ? authData.access_token : null;
    axios({
        method: 'get',
        url: `${API_BASE_URL}/users/${payload.user_id}/files/${payload.file_id}`,
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
        responseType: 'arraybuffer'
    }).then(response => {
        dispatch({
            type: FILES_SET_GETTING_FILE_DATA,
            payload: false,
        });
        let blob = new Blob([response.data])
        console.log(URL.createObjectURL(blob))
        dispatch({
            type: FILES_FILE_DATA,
            payload: blob,
        })
    }).catch( error => {
        message.show(message.type.error, 'Error while retrieving file data!')
    })
}

const duplicateFile = payload => dispatch => {
    dispatch({
        type: FILES_SET_FETCHING_ALL_FILES,
        payload: true,
    });

    return http
        .post(`${API_BASE_URL}/users/${payload.user_id}/files/${payload.file_id}/duplicate`)
        .then(() => {
            message.show(message.type.success, "Successfully duplicated file to 'My Bucket'.")
        })
        .catch(error => {
            if(error.response !== undefined) {
                message.show(message.type.error, http.getErrorMessage(error))
            }
            else {
                message.show(message.type.error, "Server could not be reached")
            }
        })
}

const deleteFile = payload => dispatch => {

    dispatch({
        type: FILES_DELETE_FILE,
        payload: payload.file_id,
    });

    return http
        .delete(`${API_BASE_URL}/users/${payload.user_id}/files/${payload.file_id}`)
        .then(() => {

        })
        .catch(error => {
            if(error.response !== undefined) {
                message.show(message.type.error, http.getErrorMessage(error))
            }
            else {
                message.show(message.type.error, "Server could not be reached")
            }
        })
};
export { fetchAllFiles, fetchStarredFiles, fetchTrashedFiles,
         uploadNewFile, updateFile, duplicateFile,
         deleteFile, downloadFile, getFileData, renameFile }