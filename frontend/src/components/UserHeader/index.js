import React from 'react'
import {Layout, Menu, Input, Dropdown} from 'antd'
import {
    SettingOutlined, LogoutOutlined, FileTextTwoTone,
    FilePdfTwoTone, FileWordTwoTone, FileExcelTwoTone,
    SoundTwoTone, VideoCameraTwoTone, FileZipTwoTone,
    FileImageTwoTone, FileUnknownTwoTone
} from "@ant-design/icons";
import logo from '../../assets/images/bucket_logo.svg'
import './style.css'

const { Header } = Layout;
const { Search } = Input;

let selectedField = '0';
let openKeys = [];

function activeComponent(history) {
    switch (history.location.pathname)
    {
        case('/user-settings'):
            selectedField = '1';
            break;
        default:
            selectedField = '0';
            break;
    }
    return selectedField;
}

class UserHeader extends React.Component {

    state = {
        collapsed: false,
        filesForSearch: [],
        filteredFiles: []
    }

    handleResize = e => {
        const windowSize = window.innerWidth;
        if(windowSize <= 900)
        {
            openKeys= []
            this.setState({
                collapsed: true,
            })
            this.forceUpdate()
        }
        else {
            this.setState({
                collapsed: false,
            })
            this.forceUpdate()
        }
    };

    componentDidMount() {
        this.setState({
            filesForSearch: this.props.filesForSearch
        })
        const windowSize = window.innerWidth;
        if(windowSize <= 900)
        {
            openKeys= []
            this.setState({
                collapsed: true,
            })
            this.forceUpdate()
        }
        else {
            this.setState({
                collapsed: false,
            })
            this.forceUpdate()
        }
        window.addEventListener("resize", this.handleResize);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize);
    }

    onOpenChange = (key) => {
        openKeys = openKeys.length === 0 ? key : []
        this.forceUpdate()
    }

    onSearchBarChange = (target) => {
        if (target === '') {
            this.setState({
                filteredFiles: this.props.filesForSearch
            })
        } else {
            this.setState({
                filteredFiles: this.state.filesForSearch.filter(item => (item.file_name + item.file_extension).toLowerCase().search(target.toLowerCase()) !== -1),
            })
        }
    }

    formatName = (name) => {
        if (name.length > 50) {
            return (name.substring(0, 47) + "...")
        } else {
            return name
        }
    }

    getIcon = (extension) => {

        switch (extension) {
            case 'json':
            case 'txt':
            case 'csv':
            case 'text':
            case 'py':
            case 'c':
            case 'cpp':
                return <FileTextTwoTone/>
            case 'pdf':
                return <FilePdfTwoTone/>
            case 'docx':
            case 'doc':
                return <FileWordTwoTone/>
            case 'xls':
            case 'xlsx':
            case 'ods':
                return <FileExcelTwoTone/>
            case 'mp3':
            case 'ogg':
            case 'wav':
                return <SoundTwoTone/>
            case 'mp4':
            case 'mov':
            case 'mkv':
                return <VideoCameraTwoTone/>
            case 'zip':
            case '7z':
            case 'rar':
                return <FileZipTwoTone/>
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'svg':
                return <FileImageTwoTone/>
            default:
                return <FileUnknownTwoTone/>
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
        if (this.state.filesForSearch.length !== nextProps.filesForSearch.length) {
            this.setState({
                filesForSearch: nextProps.filesForSearch
            })
        }
    }

    render(){
        const { history, onLogout } = this.props
        const { collapsed } = this.state
        return(
            <div style={{borderBottom: '0.75px solid #636363'}}>
                <Header className="header" >
                    <div
                        className="logo"
                        style={{width: collapsed ? 80:200,
                            margin: '0 0 0 -50px',
                            float: 'left',
                        }}
                        onClick={ () => {
                            history.push('/')
                        }}
                    >
                        <div style={{
                            marginLeft: '10px'
                        }}>
                            <img src={logo} draggable={false} style={{width: 48}} alt="Logo" />
                            { collapsed ?
                                null
                                :
                                (<strong
                                    style={{
                                        fontSize: 17,
                                        marginLeft: 3,
                                        color: '#1890ff',
                                        borderBottom: '2px solid #1890ff',
                                        borderTop: '2px solid #1890ff',
                                    }}>
                                    Drop-In-Bucket
                                </strong>)
                                }
                        </div>
                    </div>
                    <Dropdown trigger={['click']}
                              overlay={<Menu >
                                  { this.state.filteredFiles.length > 0 ? <Menu.Item key={1} icon={this.getIcon(this.state.filteredFiles[0].file_extension)}
                                     onClick={() => this.props.performSearch([this.state.filteredFiles[0]])}>
                                      {this.formatName(this.state.filteredFiles[0].file_name)}</Menu.Item> : null }
                                  { this.state.filteredFiles.length > 1 ? <Menu.Item key={2} icon={this.getIcon(this.state.filteredFiles[1].file_extension)}
                                     onClick={() => this.props.performSearch([this.state.filteredFiles[1]])}>
                                      {this.formatName(this.state.filteredFiles[1].file_name)}</Menu.Item> : null }
                                  { this.state.filteredFiles.length > 2 ? <Menu.Item key={3} icon={this.getIcon(this.state.filteredFiles[2].file_extension)}
                                     onClick={() => this.props.performSearch([this.state.filteredFiles[2]])}>
                                      {this.formatName(this.state.filteredFiles[2].file_name)}</Menu.Item> : null }
                                  { this.state.filteredFiles.length > 3 ? <Menu.Item key={4} icon={this.getIcon(this.state.filteredFiles[3].file_extension)}
                                     onClick={() => this.props.performSearch([this.state.filteredFiles[3]])}>
                                      {this.formatName(this.state.filteredFiles[3].file_name)}</Menu.Item> : null }
                              </Menu>}>
                        <Search
                            placeholder="Search in your Bucket"
                            onSearch={value => this.props.performSearch(this.state.filteredFiles)}
                            onChange={value => this.onSearchBarChange(value.target.value)}
                            style={{marginTop: 12, marginLeft: 15, width: collapsed ? '0%':'30%', float: 'left'}}
                            size={'large'}
                            enterButton />
                    </Dropdown>
                    <div style={{float: 'right', marginRight: collapsed ? -35:-15}}>
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            selectedKeys = {[activeComponent(history)]}
                            openKeys={openKeys}
                            onOpenChange={this.onOpenChange}
                        >
                            <Menu.Item
                                key="1"
                                onClick={() => {
                                    history.push('/user-settings')
                                }}
                            >
                                <SettingOutlined />
                            </Menu.Item>
                            <Menu.Item
                                key="2"
                                onClick={onLogout}
                            >
                                <LogoutOutlined />
                            </Menu.Item>
                        </Menu>
                    </div>
                </Header>
            </div>
        )}
}

export default UserHeader
