import React from 'react'

const Link = ({ to, history, children }) => (
    <a
        href={to}
        onClick={event => {
            event.preventDefault();
            history.push(to)
        }}>
        {children}
    </a>
);

export default Link