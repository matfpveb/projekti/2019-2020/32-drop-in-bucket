import React from 'react'
import { Layout, Menu, Button, Progress, Upload} from 'antd'
import { HomeOutlined,StarOutlined,DeleteOutlined,CloudUploadOutlined} from "@ant-design/icons";
import './style.css'
let selectedField = '1';
let openKeys = [];

function activeComponent(history) {
    switch (history.location.pathname)
    {
        case('/'):
            selectedField = '1';
            break;
        case('/starred'):
            selectedField = '2';
            break;
        case('/trash'):
            selectedField = '3';
            break;
        default:
            selectedField = '0';
            break;
    }
    return selectedField;
}

class Sidebar extends React.Component {

    state = {
        collapsed: window.innerWidth <= 900,
    }

    handleResize = e => {
        const windowSize = window.innerWidth;
        if(windowSize <= 900)
        {
            openKeys= []
            this.setState({
                collapsed: true,
            })
            this.forceUpdate()
        }
        else {
            this.setState({
                collapsed: false,
            })
            this.forceUpdate()
        }
    };

    componentDidMount() {
        const windowSize = window.innerWidth;
        if(windowSize <= 900)
        {
            openKeys= []
            this.setState({
                collapsed: true,
            })
            this.forceUpdate()
        }
        else {
            this.setState({
                collapsed: false,
            })
            this.forceUpdate()
        }
        window.addEventListener("resize", this.handleResize);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize);
    }

    onOpenChange = (key) => {
        openKeys = openKeys.length === 0 ? key : []
        this.forceUpdate()
    }

    formatBytes = (a,b= 2) => {
        if(0===a)
            return"0 Bytes";
        const c = 0 > b ? 0:b, d = Math.floor(Math.log(a)/Math.log(1024));
        return parseFloat((a/Math.pow(1024,d)).toFixed(c))+" "+["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"][d]
    }

    render(){
        const { history } = this.props
        const { storage } = this.props
        const { collapsed } = this.state
        return(
            <Layout.Sider
                collapsed={collapsed}
                style={{
                    overflow: 'hidden',
                    minHeight: 'calc(100vh - 65px)',
                }}
            >
                    <div align={'center'} style={{marginTop: 15, marginBottom:15}}>
                        <Upload
                            name = {'file'}
                            beforeUpload={(file) => {
                                this.props.triggerSidebarUpload(file)
                                return false;
                            }}
                            multiple = {false}
                            showUploadList = {false}
                        >
                            <Button
                                className={'bucket-upload-button'}
                                style={{ width: collapsed ? 65:90 }}
                                type={'primary'}
                                shape="round"
                                icon={<CloudUploadOutlined style={{fontSize: collapsed ? 20:28, marginTop: collapsed ? 4:0}}/>}
                                size={'large'}
                            />
                        </Upload>
                    </div>

                    <Menu
                        theme="dark"
                        mode="inline"
                        selectedKeys = {[activeComponent(history)]}
                        openKeys={openKeys}
                        onOpenChange={this.onOpenChange}
                        style={{
                            borderRight: 0,
                            borderBottom: collapsed ? 0 : '0.75px solid #636363'
                        }}
                    >
                        <Menu.Item
                            key="1"
                            onClick={() => {
                                history.push('/')
                            }}>
                            <HomeOutlined />
                            <span className="nav-text">My Bucket</span>
                        </Menu.Item>
                        <Menu.Item
                            key="2"
                            onClick={() => {
                                history.push('/starred')
                            }}>
                            <StarOutlined />
                            <span className="nav-text">Starred</span>
                        </Menu.Item>
                        <Menu.Item
                            key="3"
                            onClick={() => {
                                history.push('/trash')
                            }}>
                            <DeleteOutlined />
                            <span className="nav-text">Trash</span>
                        </Menu.Item>
                    </Menu>
                    {collapsed ? null :
                    <div style={{marginTop: 15, marginLeft: 25, marginRight: 12}}>
                        <div style={{fontSize: 13}}>
                            {this.formatBytes(storage)} of 1 GB used
                        </div>
                        <Progress percent={((storage/1048576)/1024)*100} size="small" showInfo={false}/>
                    </div>
                    }
            </Layout.Sider>
        )}
}

export default Sidebar
